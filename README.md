# Stream scheduler
Telegram robot which main job is to notify about twich/torovo streams

## How can it be used
- As a stream calendar (commands /addevent, /events, etc)
- As a directory of streamers

All events and streamers are bound to chats and can not be seen outside of chat they was added.

## How to use:
1. Build
  ```
  cargo build --release
  cp target/release/stream-scheduler /usr/bin/stream-scheduler
  ```
2. Check requred env variables:
  ```
  stream-scheduler config
  ```
3. Set required variables
4. Run
  ```
  stream-scheduler run
  ```

All data can be modified as from TG interface(check `/help` command) and from CLI interface, check `stream-scheduler db` commands
