use crate::proto::common::ITimeGetter;
use chrono::{DateTime, Utc};

pub struct MockTimeGetter {
    pub now: DateTime<Utc>,
}

impl ITimeGetter for MockTimeGetter {
    fn now(&self) -> DateTime<Utc> {
        self.now.clone()
    }
}
