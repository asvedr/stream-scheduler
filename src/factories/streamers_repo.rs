use crate::entities::errors::StreamerRepoError;
use crate::entities::streamers::{NewStreamer, Streamer};
use crate::factories::queue::Queue;
use crate::proto::streamers::IStreamersRepo;

pub struct MockStreamersRepo {
    pub register: Queue<NewStreamer, Result<i64, StreamerRepoError>>,
    pub remove: Queue<i64, Result<(), StreamerRepoError>>,
    pub get_by_name: Queue<(i64, String), Result<Streamer, StreamerRepoError>>,
    pub get_all_for_chat: Queue<i64, Result<Vec<Streamer>, StreamerRepoError>>,
    pub get_by_tag: Queue<(Option<i64>, String), Result<Vec<Streamer>, StreamerRepoError>>,
    pub get_all: Queue<(), Result<Vec<Streamer>, StreamerRepoError>>,
    pub get_by_id: Queue<i64, Result<Streamer, StreamerRepoError>>,
}

impl Default for MockStreamersRepo {
    fn default() -> Self {
        Self {
            register: Queue::new("MockStreamersRepo.register"),
            remove: Queue::new("MockStreamersRepo.remove"),
            get_by_name: Queue::new("MockStreamersRepo.get_by_name"),
            get_all_for_chat: Queue::new("MockStreamersRepo.get_all_for_chat"),
            get_by_tag: Queue::new("MockStreamersRepo.get_by_tag"),
            get_all: Queue::new("MockStreamersRepo.get_all"),
            get_by_id: Queue::new("MockStreamersRepo.get_by_id"),
        }
    }
}

impl IStreamersRepo for MockStreamersRepo {
    fn register(&self, streamer: NewStreamer) -> Result<i64, StreamerRepoError> {
        self.register.pop_debug(streamer)
    }

    fn remove(&self, streamer_id: i64) -> Result<(), StreamerRepoError> {
        self.remove.pop_debug(streamer_id)
    }

    fn get_by_id(&self, id: i64) -> Result<Streamer, StreamerRepoError> {
        self.get_by_id.pop_debug(id)
    }

    fn get_by_name(&self, chat: i64, name: &str) -> Result<Streamer, StreamerRepoError> {
        self.get_by_name.pop_debug((chat, name.to_string()))
    }

    fn get_all_for_chat(&self, chat: i64) -> Result<Vec<Streamer>, StreamerRepoError> {
        self.get_all_for_chat.pop_debug(chat)
    }

    fn get_all(&self) -> Result<Vec<Streamer>, StreamerRepoError> {
        self.get_all.pop_debug(())
    }

    fn get_by_tag(
        &self,
        chat: Option<i64>,
        text: &str,
    ) -> Result<Vec<Streamer>, StreamerRepoError> {
        self.get_by_tag.pop_debug((chat, text.to_string()))
    }
}
