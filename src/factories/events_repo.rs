use crate::entities::errors::EventRepoError;
use crate::entities::events::{Event, EventWithSeconds, NewEvent};
use crate::factories::queue::Queue;
use crate::proto::events::IEventRepo;

pub struct MockEventRepo {
    pub create_event: Queue<NewEvent, Result<i64, EventRepoError>>,
    pub update_event: Queue<Event, Result<(), EventRepoError>>,
    pub add_notif_times: Queue<(i64, Vec<usize>), Result<(), EventRepoError>>,
    pub remove_event: Queue<i64, Result<(), EventRepoError>>,
    pub get_by_id: Queue<i64, Result<Event, EventRepoError>>,
    pub get_chat_events: Queue<(i64, bool), Result<Vec<Event>, EventRepoError>>,
    pub get_events_to_notify: Queue<(), Result<Vec<EventWithSeconds>, EventRepoError>>,
    pub set_send_notifications: Queue<(i64, Vec<usize>), Result<(), EventRepoError>>,
    pub remove_old_events: Queue<(), Result<(), EventRepoError>>,
}

impl Default for MockEventRepo {
    fn default() -> Self {
        Self {
            create_event: Queue::new("MockEventRepo.create_event"),
            update_event: Queue::new("MockEventRepo.update_event"),
            add_notif_times: Queue::new("MockEventRepo.add_notif_times"),
            remove_event: Queue::new("MockEventRepo.remove_event"),
            get_by_id: Queue::new("MockEventRepo.get_by_id"),
            get_chat_events: Queue::new("MockEventRepo.get_chat_events"),
            get_events_to_notify: Queue::new("MockEventRepo.get_events_to_notify"),
            set_send_notifications: Queue::new("MockEventRepo.set_send_notifications"),
            remove_old_events: Queue::new("MockEventRepo.remove_old_events"),
        }
    }
}

impl IEventRepo for MockEventRepo {
    fn create_event(&self, event: NewEvent) -> Result<i64, EventRepoError> {
        self.create_event.pop_debug(event)
    }

    fn update_event(&self, event: Event) -> Result<(), EventRepoError> {
        self.update_event.pop_debug(event)
    }

    fn add_notif_times(
        &self,
        event_id: i64,
        seconds_before: &[usize],
    ) -> Result<(), EventRepoError> {
        self.add_notif_times
            .pop_debug((event_id, seconds_before.to_vec()))
    }

    fn remove_event(&self, event_id: i64) -> Result<(), EventRepoError> {
        self.remove_event.pop_debug(event_id)
    }

    fn get_by_id(&self, event_id: i64) -> Result<Event, EventRepoError> {
        self.get_by_id.pop_debug(event_id)
    }

    fn get_chat_events(
        &self,
        chat_id: i64,
        filter_expired: bool,
    ) -> Result<Vec<Event>, EventRepoError> {
        self.get_chat_events.pop_debug((chat_id, filter_expired))
    }

    fn get_events_to_notify(&self) -> Result<Vec<EventWithSeconds>, EventRepoError> {
        self.get_events_to_notify.pop_debug(())
    }

    fn set_send_notifications(
        &self,
        event_id: i64,
        seconds_before: &[usize],
    ) -> Result<(), EventRepoError> {
        self.set_send_notifications
            .pop_debug((event_id, seconds_before.to_vec()))
    }

    fn remove_old_events(&self) -> Result<(), EventRepoError> {
        self.remove_old_events.pop_debug(())
    }
}
