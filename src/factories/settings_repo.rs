use crate::entities::errors::SettingsRepoError;
use crate::entities::settings::Settings;
use crate::factories::queue::Queue;
use crate::proto::settings::ISettingsRepo;

pub struct MockSettingsRepo {
    pub get: Queue<(), Result<Settings, SettingsRepoError>>,
    pub get_raw: Queue<(), Result<Vec<(String, String)>, SettingsRepoError>>,
    pub update: Queue<Settings, Result<(), SettingsRepoError>>,
    pub raw_to_settings: Queue<Vec<(String, String)>, Result<Settings, SettingsRepoError>>,
    pub settings_to_raw: Queue<Settings, Vec<(String, String)>>,
}

impl Default for MockSettingsRepo {
    fn default() -> Self {
        Self {
            get: Queue::new("MockSettingsRepo.get"),
            get_raw: Queue::new("MockSettingsRepo.get_raw"),
            update: Queue::new("MockSettingsRepo.update"),
            raw_to_settings: Queue::new("MockSettingsRepo.raw_to_settings"),
            settings_to_raw: Queue::new("MockSettingsRepo.settings_to_raw"),
        }
    }
}

impl ISettingsRepo for MockSettingsRepo {
    fn get(&self) -> Result<Settings, SettingsRepoError> {
        self.get.pop_debug(())
    }

    fn get_raw(&self) -> Result<Vec<(String, String)>, SettingsRepoError> {
        self.get_raw.pop_debug(())
    }

    fn update(&self, settings: Settings) -> Result<(), SettingsRepoError> {
        self.update.pop_debug(settings)
    }

    fn raw_to_settings(&self, raw: Vec<(String, String)>) -> Result<Settings, SettingsRepoError> {
        self.raw_to_settings.pop_debug(raw)
    }

    fn settings_to_raw(&self, settings: Settings) -> Vec<(String, String)> {
        self.settings_to_raw.pop_debug(settings)
    }
}
