use std::cell::RefCell;
use std::fmt::Debug;

pub struct Queue<A, R> {
    name: String,
    calls: RefCell<Vec<Box<dyn FnOnce(A) -> Option<R>>>>,
    called: RefCell<usize>,
}

impl<A: Eq + 'static, R: 'static> Queue<A, R> {
    pub fn push_eq(&self, arg: A, res: R) -> &Self {
        let fun = move |got: A| {
            if got == arg {
                Some(res)
            } else {
                None
            }
        };
        self.calls.borrow_mut().push(Box::new(fun));
        self
    }
}

impl<A: Debug + 'static, R: 'static> Queue<A, R> {
    pub fn pop_debug(&self, arg: A) -> R {
        let mut calls = self.calls.borrow_mut();
        if calls.is_empty() {
            panic!("queue {:?}: no calls expected", self.name)
        }
        let fun = calls.remove(0);
        let str_arg = format!("{:?}", arg);
        if let Some(val) = fun(arg) {
            return val;
        }
        panic!("queue {:?}: invalid call: {}", self.name, str_arg)
    }
}

impl<A: 'static, R: 'static> Queue<A, R> {
    pub fn new<T: ToString>(name: T) -> Self {
        Queue {
            name: name.to_string(),
            calls: RefCell::new(Vec::new()),
            called: RefCell::new(0),
        }
    }

    pub fn push_unchecked(&self, res: R) -> &Self {
        self.calls.borrow_mut().push(Box::new(move |_| Some(res)));
        self
    }

    pub fn push_map<F: FnOnce(A) -> Option<R> + 'static>(&self, fun: F) -> &Self {
        self.calls.borrow_mut().push(Box::new(fun));
        self
    }

    pub fn push_checked<F: Fn(A) -> bool + 'static>(&self, fun: F, res: R) -> &Self {
        let predicate = move |arg| {
            if fun(arg) {
                Some(res)
            } else {
                None
            }
        };
        self.calls.borrow_mut().push(Box::new(predicate));
        self
    }

    pub fn pop(&self, arg: A) -> R {
        let mut calls = self.calls.borrow_mut();
        if calls.is_empty() {
            panic!("queue {:?}: no calls expected", self.name)
        }
        let fun = calls.remove(0);
        if let Some(val) = fun(arg) {
            return val;
        }
        panic!("queue {:?}: invalid call", self.name)
    }

    pub fn call_count(&self) -> usize {
        *self.called.borrow()
    }
}
