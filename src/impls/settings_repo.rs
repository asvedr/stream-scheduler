use std::collections::HashMap;
use std::str::FromStr;

use easy_sqlite::traits::repo::{IConnection, IDbRepo, INewDbRepo};
use easy_sqlite::traits::table::ITable;
use easy_sqlite::{deescape, escape, SStr, SVec, TableManager};

use crate::entities::errors::SettingsRepoError;
use crate::entities::settings::Settings;
use crate::proto::settings::ISettingsRepo;

pub struct SettingsRepo<Cnn: IConnection> {
    settings: TableManager<Cnn, SettingsTable>,
    default_value: Settings,
}

struct SettingsTable;

impl ITable for SettingsTable {
    const PK: SStr = "key";
    const NAME: SStr = "settings";
    const COLUMNS: SVec<(SStr, SStr)> = &[("key", "STRING NOT NULL"), ("value", "STRING NOT NULL")];
    const UNIQUE: SVec<SStr> = &["key"];
}

impl<Cnn: IConnection> SettingsRepo<Cnn> {
    pub fn new(cnn: Cnn) -> Self {
        Self {
            settings: TableManager::create(cnn),
            default_value: Default::default(),
        }
    }

    fn serialize_settings(settings: Settings) -> Vec<(&'static str, String)> {
        let t_zones = settings
            .timezones
            .into_iter()
            .map(|(k, v)| format!("{}:{}", k, v))
            .collect::<Vec<_>>()
            .join(",");
        let shifts = settings
            .notif_shifts_in_minutes
            .into_iter()
            .map(|val| val.to_string())
            .collect::<Vec<_>>()
            .join(",");
        vec![
            ("timezones", t_zones),
            ("default_timezone", settings.default_timezone),
            ("local_timezone", settings.local_timezone),
            ("display_timezones", settings.display_timezones.join(",")),
            ("notif_shifts_in_minutes", shifts),
        ]
    }

    fn deserialize_settings(
        &self,
        rows: HashMap<String, String>,
    ) -> Result<Settings, SettingsRepoError> {
        let timezones = rows
            .get("timezones")
            .map(|s| Self::parse_timezones(s))
            .unwrap_or_else(|| Ok(self.default_value.timezones.clone()))?;
        let default_timezone = rows
            .get("default_timezone")
            .unwrap_or(&self.default_value.default_timezone)
            .clone();
        let local_timezone = rows
            .get("local_timezone")
            .unwrap_or(&self.default_value.local_timezone)
            .clone();
        let display_timezones = rows
            .get("display_timezones")
            .map(|s| s.split(',').map(|s| s.to_string()).collect())
            .unwrap_or_else(|| self.default_value.display_timezones.clone());
        let default_not_found = !timezones.iter().any(|(k, _)| *k == default_timezone);
        let local_not_found = !timezones.iter().any(|(k, _)| *k == local_timezone);
        if default_not_found {
            return Err(SettingsRepoError::InvalidValueAtKey("default_timezone"));
        }
        if local_not_found {
            return Err(SettingsRepoError::InvalidValueAtKey("local_timezone"));
        }
        let notif_shifts_in_minutes = rows
            .get("notif_shifts_in_minutes")
            .map(|s| Self::parse_shifts(s))
            .unwrap_or_else(|| Ok(self.default_value.notif_shifts_in_minutes.clone()))?;
        Ok(Settings {
            timezones,
            display_timezones,
            default_timezone,
            local_timezone,
            notif_shifts_in_minutes,
        })
    }

    fn parse_timezones(src: &str) -> Result<Vec<(String, i32)>, SettingsRepoError> {
        let mut result = Vec::new();
        for token in src.split(',') {
            let token = token.trim();
            if token.is_empty() {
                continue;
            }
            let pair = token.split(':').collect::<Vec<_>>();
            if pair.len() != 2 {
                return Err(SettingsRepoError::InvalidValueAtKey("timezones"));
            }
            let shift = i32::from_str(pair[1])
                .map_err(|_| SettingsRepoError::InvalidValueAtKey("timezones"))?;
            result.push((pair[0].to_string(), shift));
        }
        Ok(result)
    }

    fn parse_shifts(src: &str) -> Result<Vec<usize>, SettingsRepoError> {
        src.split(',')
            .filter(|token| !token.is_empty())
            .map(usize::from_str)
            .collect::<Result<Vec<usize>, _>>()
            .map_err(|_| SettingsRepoError::InvalidValueAtKey("notif_shifts_in_minutes"))
    }
}

impl<Cnn: IConnection> IDbRepo for SettingsRepo<Cnn> {
    fn as_super(&self) -> &dyn IDbRepo {
        &self.settings
    }
}

impl<Cnn: IConnection> ISettingsRepo for SettingsRepo<Cnn> {
    fn get(&self) -> Result<Settings, SettingsRepoError> {
        let rows = self.get_raw()?;
        self.deserialize_settings(rows.into_iter().collect())
    }

    fn get_raw(&self) -> Result<Vec<(String, String)>, SettingsRepoError> {
        let rows: Vec<(String, String)> = self.settings.get_many(
            "SELECT `key`, `value` FROM settings ORDER BY `key`",
            |row| Ok((row.get(0)?, deescape(row.get(1)?))),
        )?;
        Ok(rows)
    }

    fn update(&self, settings: Settings) -> Result<(), SettingsRepoError> {
        let values = Self::serialize_settings(settings)
            .into_iter()
            .map(|(k, v)| format!("(\'{}\', {})", k, escape(&v)))
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            "INSERT OR REPLACE INTO {tbl} (`key`, `value`) VALUES {values}",
            tbl = SettingsTable::NAME,
            values = values
        );
        self.settings.execute(&query)?;
        Ok(())
    }

    fn raw_to_settings(&self, raw: Vec<(String, String)>) -> Result<Settings, SettingsRepoError> {
        self.deserialize_settings(raw.into_iter().collect())
    }

    fn settings_to_raw(&self, settings: Settings) -> Vec<(String, String)> {
        Self::serialize_settings(settings)
            .into_iter()
            .map(|(k, v)| (k.to_string(), v))
            .collect()
    }
}
