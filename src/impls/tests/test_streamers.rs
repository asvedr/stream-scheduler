use easy_sqlite::traits::repo::IDbRepo;
use easy_sqlite::RSQLConnection;

use crate::entities::errors::StreamerRepoError;
use crate::entities::streamers::NewStreamer;
use crate::impls::streamers_repo::StreamersRepo;
use crate::proto::streamers::IStreamersRepo;

fn make_repo() -> StreamersRepo<RSQLConnection> {
    let cnn = RSQLConnection::new(":memory:").unwrap();
    let repo = StreamersRepo::new(cnn);
    repo.init().unwrap();
    repo
}

#[test]
fn test_get_none() {
    let repo = make_repo();
    assert_eq!(
        repo.get_by_name(1, "abc").unwrap_err(),
        StreamerRepoError::NotFound,
    )
}

#[test]
fn test_register_remove() {
    let repo = make_repo();
    // no error, no action
    repo.remove(1).unwrap();
    let new = NewStreamer {
        name: "abc".to_string(),
        chat: 1,
        tags: vec!["hey".to_string(), "hop".to_string()],
        links: vec!["lala".to_string(), "lay".to_string()],
    };
    repo.register(new.clone()).unwrap();
    assert_eq!(
        repo.get_by_name(2, "abc").unwrap_err(),
        StreamerRepoError::NotFound,
    );
    let streamer = repo.get_by_name(1, "abc").unwrap();
    assert_eq!(streamer.new, new);

    repo.remove(streamer.id).unwrap();

    assert_eq!(
        repo.get_by_name(2, "abc").unwrap_err(),
        StreamerRepoError::NotFound,
    );
}

#[test]
fn test_get_all_for_chat() {
    let repo = make_repo();
    repo.register(NewStreamer {
        name: "abc".to_string(),
        chat: 1,
        tags: vec!["hey".to_string(), "hop".to_string()],
        links: vec!["lala".to_string(), "lay".to_string()],
    })
    .unwrap();
    repo.register(NewStreamer {
        name: "cba".to_string(),
        chat: 1,
        tags: vec!["bee".to_string()],
        links: vec!["lala".to_string(), "lay".to_string()],
    })
    .unwrap();

    assert!(repo.get_all_for_chat(2).unwrap().is_empty());

    assert_eq!(repo.get_all_for_chat(1).unwrap().len(), 2);
}

#[test]
fn test_get_by_tag() {
    let repo = make_repo();
    repo.register(NewStreamer {
        name: "abc".to_string(),
        chat: 1,
        tags: vec!["hey".to_string(), "hop".to_string()],
        links: vec!["lala".to_string(), "lay".to_string()],
    })
    .unwrap();
    repo.register(NewStreamer {
        name: "cba".to_string(),
        chat: 1,
        tags: vec!["hey".to_string()],
        links: vec!["lala".to_string(), "lay".to_string()],
    })
    .unwrap();
    repo.register(NewStreamer {
        name: "bac".to_string(),
        chat: 3,
        tags: vec!["hop".to_string()],
        links: vec![],
    })
    .unwrap();
    assert!(repo.get_by_tag(Some(2), "hey").unwrap().is_empty());
    assert_eq!(repo.get_by_tag(Some(1), "hey").unwrap().len(), 2);
    assert_eq!(repo.get_by_tag(Some(1), "hop").unwrap().len(), 1);
    assert_eq!(repo.get_by_tag(None, "hop").unwrap().len(), 2);

    assert_eq!(repo.get_all().unwrap().len(), 3);
}

#[test]
fn test_insert_no_tags() {
    let repo = make_repo();
    repo.register(NewStreamer {
        name: "abc".to_string(),
        chat: 1,
        tags: vec![],
        links: vec![],
    })
    .unwrap();
}
