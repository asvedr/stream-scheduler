use chrono::{DateTime, Duration, NaiveDate, Timelike, Utc};
use easy_sqlite::traits::repo::IDbRepo;
use easy_sqlite::RSQLConnection;

use crate::entities::errors::EventRepoError;
use crate::entities::events::{Event, NewEvent};
use crate::impls::events_repo::EventRepo;
use crate::proto::events::IEventRepo;

fn make_repo() -> EventRepo<RSQLConnection> {
    let cnn = RSQLConnection::new(":memory:").unwrap();
    let repo = EventRepo::new(cnn);
    repo.init().unwrap();
    repo
}

fn future() -> DateTime<Utc> {
    let dt = NaiveDate::from_ymd_opt(4020, 1, 1)
        .unwrap()
        .and_hms_opt(1, 2, 3)
        .unwrap();
    DateTime::from_utc(dt, Utc)
}

fn past() -> DateTime<Utc> {
    let dt = NaiveDate::from_ymd_opt(2020, 1, 1)
        .unwrap()
        .and_hms_opt(1, 2, 3)
        .unwrap();
    DateTime::from_utc(dt, Utc)
}

#[test]
fn test_get_none() {
    let repo = make_repo();
    assert_eq!(repo.get_by_id(0), Err(EventRepoError::NotFound),);
    assert_eq!(repo.get_chat_events(1, true), Ok(Vec::new()))
}

#[test]
fn test_get() {
    let repo = make_repo();
    let new = NewEvent {
        name: "aaa".to_string(),
        chat_id: 1,
        timezone: "msk".to_string(),
        time: future(),
        message: "hoho".to_string(),
    };
    let id = repo.create_event(new.clone()).unwrap();
    let events = repo.get_chat_events(new.chat_id, true).unwrap();
    assert_eq!(events.len(), 1);
    assert_eq!(events[0].id, id);
    assert_eq!(events[0].new, new);
    let got = repo.get_by_id(id).unwrap();
    assert_eq!(got.new, new);
    repo.remove_event(got.id).unwrap();
    assert_eq!(repo.get_chat_events(new.chat_id, true), Ok(Vec::new()))
}

#[test]
fn test_update_event() {
    let repo = make_repo();
    let new = NewEvent {
        name: "aaa".to_string(),
        chat_id: 1,
        timezone: "msk".to_string(),
        time: future(),
        message: "hoho".to_string(),
    };
    let id = repo.create_event(new.clone()).unwrap();
    let mut event = Event { id, new };
    event.new.message = "hehe".to_string();
    repo.update_event(event.clone()).unwrap();
    let got = repo.get_by_id(event.id).unwrap();
    assert_eq!(event, got);
}

#[test]
fn test_add_notif_times_n_get_ready() {
    let repo = make_repo();
    let now = Utc::now().with_nanosecond(0).unwrap();
    let target_time = now + Duration::minutes(10);

    assert!(repo.get_events_to_notify().unwrap().is_empty());

    let new = NewEvent {
        name: "aaa".to_string(),
        chat_id: 1,
        timezone: "msk".to_string(),
        time: target_time,
        message: "hoho".to_string(),
    };
    let event_id = repo.create_event(new.clone()).unwrap();

    assert!(repo.get_events_to_notify().unwrap().is_empty());

    // 20min, 15min, 6min, 1min
    let times = &[60 * 20, 60 * 15, 60 * 5, 60];

    repo.add_notif_times(event_id, times).unwrap();
    let events = repo.get_events_to_notify().unwrap();
    assert_eq!(events.len(), 1);
    assert_eq!(events[0].event.new, new);
    assert_eq!(events[0].seconds_before, &[60 * 15, 60 * 20]);

    repo.set_send_notifications(event_id, &[60 * 20]).unwrap();

    let events = repo.get_events_to_notify().unwrap();
    assert_eq!(events.len(), 1);
    assert_eq!(events[0].event.new, new);
    assert_eq!(events[0].seconds_before, &[60 * 15]);

    repo.set_send_notifications(event_id, &[60 * 15]).unwrap();

    assert!(repo.get_events_to_notify().unwrap().is_empty());
}

#[test]
fn test_do_not_remove_future() {
    let repo = make_repo();
    let id = repo
        .create_event(NewEvent {
            name: "aaa".to_string(),
            chat_id: 1,
            timezone: "msk".to_string(),
            time: future(),
            message: "hoho".to_string(),
        })
        .unwrap();
    repo.remove_old_events().unwrap();
    // no errors
    repo.get_by_id(id).unwrap();
    repo.add_notif_times(id, &[10]).unwrap();
    repo.set_send_notifications(id, &[10]).unwrap();
    repo.remove_old_events().unwrap();
    // no errors
    repo.get_by_id(id).unwrap();
}

#[test]
fn test_do_not_remove_old_if_not_sent() {
    let repo = make_repo();
    let id = repo
        .create_event(NewEvent {
            name: "aaa".to_string(),
            chat_id: 1,
            timezone: "msk".to_string(),
            time: past(),
            message: "hoho".to_string(),
        })
        .unwrap();
    repo.add_notif_times(id, &[10]).unwrap();
    repo.remove_old_events().unwrap();
    // no errors
    repo.get_by_id(id).unwrap();
}

#[test]
fn test_remove_old_sent() {
    let repo = make_repo();
    let id = repo
        .create_event(NewEvent {
            name: "aaa".to_string(),
            chat_id: 1,
            timezone: "msk".to_string(),
            time: past(),
            message: "hoho".to_string(),
        })
        .unwrap();
    repo.add_notif_times(id, &[10]).unwrap();
    repo.set_send_notifications(id, &[10]).unwrap();
    repo.remove_old_events().unwrap();
    // no errors
    assert_eq!(repo.get_by_id(id).unwrap_err(), EventRepoError::NotFound)
}

#[test]
fn test_remove_old_no_notifs() {
    let repo = make_repo();
    let id = repo
        .create_event(NewEvent {
            name: "aaa".to_string(),
            chat_id: 1,
            timezone: "msk".to_string(),
            time: past(),
            message: "hoho".to_string(),
        })
        .unwrap();
    repo.remove_old_events().unwrap();
    // no errors
    assert_eq!(repo.get_by_id(id).unwrap_err(), EventRepoError::NotFound)
}

#[test]
fn test_issues_4() {
    let repo = make_repo();
    let a_id = repo
        .create_event(NewEvent {
            name: "aaa".to_string(),
            chat_id: 1,
            timezone: "utc".to_string(),
            time: Utc::now() + Duration::minutes(19),
            message: "abc".to_string(),
        })
        .unwrap();
    repo.add_notif_times(a_id, &[20 * 60, 10 * 60, 5 * 60])
        .unwrap();
    let b_id = repo
        .create_event(NewEvent {
            name: "bbb".to_string(),
            chat_id: 1,
            timezone: "utc".to_string(),
            time: Utc::now() + Duration::hours(12),
            message: "cab".to_string(),
        })
        .unwrap();
    repo.add_notif_times(b_id, &[20 * 60, 10 * 60, 5 * 60])
        .unwrap();
    let events = repo.get_events_to_notify().unwrap();
    assert_eq!(events.len(), 1);
    assert_eq!(events[0].event.new.name, "aaa");
}
