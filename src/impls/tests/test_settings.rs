use crate::entities::errors::SettingsRepoError;
use crate::entities::settings::Settings;
use crate::impls::settings_repo::SettingsRepo;
use crate::proto::settings::ISettingsRepo;
use easy_sqlite::traits::repo::IDbRepo;
use easy_sqlite::RSQLConnection;

fn make_repo() -> SettingsRepo<RSQLConnection> {
    let cnn = RSQLConnection::new(":memory:").unwrap();
    let repo = SettingsRepo::new(cnn);
    repo.init().unwrap();
    repo
}

#[test]
fn test_get_default() {
    let repo = make_repo();
    assert_eq!(repo.get(), Ok(Settings::default()));
    assert!(repo.get_raw().unwrap().is_empty());
}

#[test]
fn test_update() {
    let repo = make_repo();
    let settings = Settings {
        timezones: vec![("a".to_string(), 1), ("b".to_string(), -1)],
        display_timezones: vec!["b".to_string(), "a".to_string()],
        default_timezone: "a".to_string(),
        local_timezone: "b".to_string(),
        notif_shifts_in_minutes: vec![1, 2, 3],
    };
    repo.update(settings.clone()).unwrap();
    assert_eq!(repo.get(), Ok(settings));
    let expected = vec![
        ("default_timezone".to_string(), "a".to_string()),
        ("display_timezones".to_string(), "b,a".to_string()),
        ("local_timezone".to_string(), "b".to_string()),
        ("notif_shifts_in_minutes".to_string(), "1,2,3".to_string()),
        ("timezones".to_string(), "a:1,b:-1".to_string()),
    ];
    assert_eq!(repo.get_raw().unwrap(), expected);
}

#[test]
fn construct_deconstruct_ok() {
    let repo = make_repo();
    let settings = repo.get().unwrap();
    let rows = repo.settings_to_raw(settings.clone());
    assert_eq!(repo.raw_to_settings(rows), Ok(settings));
}

#[test]
fn construct_deconstruct_err() {
    let repo = make_repo();
    let settings = repo.get().unwrap();
    let mut rows = repo
        .settings_to_raw(settings)
        .into_iter()
        .filter(|(k, _)| k == "default_timezone")
        .collect::<Vec<_>>();
    rows.push(("default_timezone".to_string(), "abc".to_string()));
    assert_eq!(
        repo.raw_to_settings(rows),
        Err(SettingsRepoError::InvalidValueAtKey("default_timezone")),
    );
}
