use crate::impls::time_ui::TimeUI;
use crate::proto::common::ITimeUI;
use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

fn make_parser() -> TimeUI {
    TimeUI
}

fn default_date() -> NaiveDate {
    NaiveDate::from_ymd_opt(2020, 1, 1).unwrap()
}

#[test]
fn test_full() {
    let parser = make_parser();
    let (time, tz) = parser
        .parse_time("2134-3-02 11:30 Cy", default_date(), "msk")
        .unwrap();
    let expected = NaiveDateTime::new(
        NaiveDate::from_ymd_opt(2134, 3, 2).unwrap(),
        NaiveTime::from_hms_opt(11, 30, 0).unwrap(),
    );
    assert_eq!(time, expected);
    assert_eq!(tz, "cy");
}

#[test]
fn test_no_year() {
    let parser = make_parser();
    let (time, tz) = parser
        .parse_time("3-2 11:30 Cy", default_date(), "msk")
        .unwrap();
    let expected = NaiveDateTime::new(
        NaiveDate::from_ymd_opt(2020, 3, 2).unwrap(),
        NaiveTime::from_hms_opt(11, 30, 0).unwrap(),
    );
    assert_eq!(time, expected);
    assert_eq!(tz, "cy");
}

#[test]
fn test_no_year_no_month() {
    let parser = make_parser();
    let (time, tz) = parser
        .parse_time("2 11:30 Cy", default_date(), "msk")
        .unwrap();
    let expected = NaiveDateTime::new(
        NaiveDate::from_ymd_opt(2020, 1, 2).unwrap(),
        NaiveTime::from_hms_opt(11, 30, 0).unwrap(),
    );
    assert_eq!(time, expected);
    assert_eq!(tz, "cy");
}

#[test]
fn test_no_year_no_month_no_tz() {
    let parser = make_parser();
    let (time, tz) = parser.parse_time("2 11:30", default_date(), "msk").unwrap();
    let expected = NaiveDateTime::new(
        NaiveDate::from_ymd_opt(2020, 1, 2).unwrap(),
        NaiveTime::from_hms_opt(11, 30, 0).unwrap(),
    );
    assert_eq!(time, expected);
    assert_eq!(tz, "msk");
}

#[test]
fn test_bare_time() {
    let parser = make_parser();
    let (time, tz) = parser.parse_time("11:30", default_date(), "msk").unwrap();
    let expected = NaiveDateTime::new(
        NaiveDate::from_ymd_opt(2020, 1, 1).unwrap(),
        NaiveTime::from_hms_opt(11, 30, 0).unwrap(),
    );
    assert_eq!(time, expected);
    assert_eq!(tz, "msk");
}

#[test]
fn test_day_shift() {
    let parser = make_parser();
    let (time, tz) = parser
        .parse_time("послезавтра 11:30", default_date(), "msk")
        .unwrap();
    let expected = NaiveDateTime::new(
        NaiveDate::from_ymd_opt(2020, 1, 3).unwrap(),
        NaiveTime::from_hms_opt(11, 30, 0).unwrap(),
    );
    assert_eq!(time, expected);
    assert_eq!(tz, "msk");
}
