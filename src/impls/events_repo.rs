use std::collections::HashMap;

use chrono::Utc;
use easy_sqlite::errors::DbResult;
use easy_sqlite::traits::repo::{IConnection, IDbRepo, INewDbRepo};
use easy_sqlite::traits::table::ITable;
use easy_sqlite::{deescape, escape, SStr, SVec, TableManager};
use rusqlite::Connection;

use crate::entities::errors::EventRepoError;
use crate::entities::events::{Event, EventWithSeconds, NewEvent};
use crate::proto::events::IEventRepo;
use crate::utils::map_tools;
use crate::utils::time::{int_to_utc, utc_to_int};

pub struct EventRepo<Cnn: IConnection> {
    events: TableManager<Cnn, EventTable>,
    notifs: TableManager<Cnn, NotifTable>,

    events_select: String,
    events_insert: String,
    notifs_insert: String,
}

struct EventTable;
struct NotifTable;

impl ITable for EventTable {
    const PK: SStr = "id";
    const NAME: SStr = "events";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("chat_id", "INTEGER NOT NULL"),
        ("name", "STRING"),
        ("message", "STRING"),
        ("timezone", "STRING"),
        ("time", "INTEGER"),
    ];
    const INDEXES: SVec<SStr> = &["name", "chat_id"];
    const UNIQUE: SVec<SStr> = &["name,chat_id"];
}

impl ITable for NotifTable {
    const PK: SStr = "id";
    const NAME: SStr = "notifs";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("event_id", "INTEGER NOT NULL"),
        ("seconds_before", "INTEGER NOT NULL"),
        ("sent", "INTEGER NOT NULL DEFAULT 0"),
    ];
    const INDEXES: SVec<SStr> = &["event_id"];
    const UNIQUE: SVec<SStr> = &["event_id,seconds_before"];
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> =
        &[("event_id", EventTable::NAME, EventTable::PK)];
}

impl<Cnn: IConnection> IDbRepo for EventRepo<Cnn> {
    fn init(&self) -> DbResult<()> {
        self.events.init()?;
        self.notifs.init()
    }
    fn drop(&self) -> DbResult<()> {
        self.notifs.drop()?;
        self.events.drop()
    }
    fn set_indexes(&self) -> DbResult<()> {
        self.events.set_indexes()?;
        self.notifs.set_indexes()
    }
    fn drop_indexes(&self) -> DbResult<()> {
        self.events.drop_indexes()?;
        self.notifs.drop_indexes()
    }
    fn get_size(&self) -> DbResult<usize> {
        self.events.get_size()
    }
}

impl<Cnn: IConnection + Clone> EventRepo<Cnn> {
    pub fn new(cnn: Cnn) -> Self {
        Self {
            events: TableManager::create(cnn.clone()),
            notifs: TableManager::create(cnn),
            events_select: EventTable::get_columns_as_str(&[]),
            events_insert: EventTable::get_columns_as_str(&[EventTable::PK]),
            notifs_insert: NotifTable::get_columns_as_str(&[NotifTable::PK, "sent"]),
        }
    }
}

impl<Cnn: IConnection> EventRepo<Cnn> {
    fn delete_notifs(&self, event_id: i64) -> DbResult<()> {
        let query = format!(
            "DELETE FROM {tbl} WHERE event_id = {id}",
            tbl = NotifTable::NAME,
            id = event_id
        );
        self.notifs.execute(&query)
    }

    fn deserialize_event(row: &rusqlite::Row<'_>) -> rusqlite::Result<Event> {
        let event = Event {
            id: row.get(0)?,
            new: NewEvent {
                chat_id: row.get(1)?,
                name: deescape(row.get(2)?),
                message: deescape(row.get(3)?),
                timezone: deescape(row.get(4)?),
                time: int_to_utc(row.get(5)?),
            },
        };
        Ok(event)
    }

    fn get_events_to_notify_fun(&self, cnn: &Connection) -> DbResult<Vec<EventWithSeconds>> {
        let now = Utc::now().timestamp();
        let query = format!(
            r#"
                SELECT n.event_id, n.seconds_before
                FROM {events} as e JOIN {notifs} as n ON e.id = n.event_id
                WHERE n.sent = 0 and e.time - n.seconds_before < {now}
            "#,
            events = EventTable::NAME,
            notifs = NotifTable::NAME,
            now = now,
        );
        let mut binding = cnn.prepare(&query)?;
        let rows = binding.query_map((), |row| {
            Ok((row.get::<_, i64>(0)?, row.get::<_, usize>(1)?))
        })?;
        let mut map = HashMap::new();
        for row in rows {
            let (event_id, seconds) = row?;
            map_tools::add(&mut map, event_id, seconds);
        }
        let event_id_list = map
            .keys()
            .map(|k| k.to_string())
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            r#"
            SELECT {columns} FROM {tbl} WHERE id IN ({id_list})
            ORDER BY time
            "#,
            columns = self.events_select,
            tbl = EventTable::NAME,
            id_list = event_id_list,
        );
        let mut binding = cnn.prepare(&query)?;
        let rows = binding.query_map((), Self::deserialize_event)?;
        let mut result = Vec::new();
        for row in rows {
            let event = row?;
            let mut seconds_before = map.remove(&event.id).unwrap();
            seconds_before.sort();
            result.push(EventWithSeconds {
                event,
                seconds_before,
            })
        }
        Ok(result)
    }

    fn remove_old_events_fun(&self, cnn: &Connection) -> DbResult<()> {
        let now = Utc::now().timestamp();
        let query = format!(
            r#"
                WITH grouped as (
                    SELECT e.id, COALESCE(MIN(n.sent), 1) as sent
                    FROM {events} as e LEFT JOIN {notifs} as n
                        ON e.id = n.event_id
                    WHERE e.time < {now}
                    GROUP BY e.id
                ) SELECT id FROM grouped WHERE sent = 1
            "#,
            events = EventTable::NAME,
            notifs = NotifTable::NAME,
            now = now,
        );
        let id_list = cnn
            .prepare(&query)?
            .query_map((), |row| Ok(row.get::<_, usize>(0)?.to_string()))?
            .collect::<Result<Vec<_>, _>>()?
            .join(",");
        let query = format!(
            "DELETE FROM {tbl} WHERE event_id IN ({id_list})",
            tbl = NotifTable::NAME,
            id_list = id_list,
        );
        cnn.execute(&query, ())?;
        let query = format!(
            "DELETE FROM {tbl} WHERE id IN ({id_list})",
            tbl = EventTable::NAME,
            id_list = id_list,
        );
        cnn.execute(&query, ())?;
        Ok(())
    }
}

impl<Cnn: IConnection> IEventRepo for EventRepo<Cnn> {
    fn create_event(&self, event: NewEvent) -> Result<i64, EventRepoError> {
        let query = format!(
            r#"
                INSERT INTO {tbl} ({columns})
                VALUES ({chat_id}, {name}, {message}, {timezone}, {time})
            "#,
            columns = self.events_insert,
            tbl = EventTable::NAME,
            chat_id = event.chat_id,
            name = escape(&event.name),
            message = escape(&event.message),
            timezone = escape(&event.timezone),
            time = utc_to_int(event.time)
        );
        let id = self.events.execute_return_id(&query)?;
        Ok(id)
    }

    fn update_event(&self, event: Event) -> Result<(), EventRepoError> {
        self.delete_notifs(event.id)?;
        let query = format!(
            r#"
                UPDATE {tbl}
                SET
                    chat_id={chat_id},
                    name={name},
                    message={message},
                    timezone={timezone},
                    time={time}
                WHERE id={id}
            "#,
            tbl = EventTable::NAME,
            chat_id = event.new.chat_id,
            name = escape(&event.new.name),
            message = escape(&event.new.message),
            timezone = escape(&event.new.timezone),
            time = event.new.time.timestamp(),
            id = event.id,
        );
        self.events.execute(&query)?;
        Ok(())
    }

    fn add_notif_times(
        &self,
        event_id: i64,
        seconds_before: &[usize],
    ) -> Result<(), EventRepoError> {
        let values = seconds_before
            .iter()
            .map(|seconds| format!("({}, {})", event_id, seconds))
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            r#"INSERT OR IGNORE INTO {tbl} ({columns})
            VALUES {values}"#,
            tbl = NotifTable::NAME,
            columns = self.notifs_insert,
            values = values,
        );
        self.notifs.execute(&query)?;
        Ok(())
    }

    fn remove_event(&self, event_id: i64) -> Result<(), EventRepoError> {
        self.delete_notifs(event_id)?;
        let query = format!(
            "DELETE FROM {tbl} WHERE id = {id}",
            tbl = EventTable::NAME,
            id = event_id
        );
        self.events.execute(&query)?;
        Ok(())
    }

    fn get_by_id(&self, event_id: i64) -> Result<Event, EventRepoError> {
        let query = format!(
            "SELECT {columns} FROM {tbl} WHERE id = {id}",
            columns = self.events_select,
            tbl = EventTable::NAME,
            id = event_id,
        );
        Ok(self.events.get_one(&query, Self::deserialize_event)?)
    }

    fn get_chat_events(
        &self,
        chat_id: i64,
        filter_expired: bool,
    ) -> Result<Vec<Event>, EventRepoError> {
        let now = Utc::now().timestamp();
        let time_case = if filter_expired {
            format!("time > {} AND ", now)
        } else {
            "".to_string()
        };
        let query = format!(
            r#"SELECT {columns} FROM {tbl}
               WHERE {time_case} chat_id = {chat_id}"#,
            columns = self.events_select,
            tbl = EventTable::NAME,
            time_case = time_case,
            chat_id = chat_id
        );
        Ok(self.events.get_many(&query, Self::deserialize_event)?)
    }

    fn get_events_to_notify(&self) -> Result<Vec<EventWithSeconds>, EventRepoError> {
        let val = self
            .events
            .connection
            .with(|cnn| self.get_events_to_notify_fun(cnn))?;
        Ok(val)
    }

    fn set_send_notifications(
        &self,
        event_id: i64,
        seconds_before: &[usize],
    ) -> Result<(), EventRepoError> {
        let seconds = seconds_before
            .iter()
            .map(|m| m.to_string())
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            r#"UPDATE {tbl}
                SET sent = 1
            WHERE event_id = {eid}
                AND seconds_before IN ({seconds})
            "#,
            tbl = NotifTable::NAME,
            eid = event_id,
            seconds = seconds,
        );
        self.notifs.execute(&query)?;
        Ok(())
    }

    fn remove_old_events(&self) -> Result<(), EventRepoError> {
        self.events
            .connection
            .with(|cnn| self.remove_old_events_fun(cnn))?;
        Ok(())
    }
}
