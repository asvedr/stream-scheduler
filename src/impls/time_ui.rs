use chrono::{
    DateTime, Datelike, Duration, FixedOffset, NaiveDate, NaiveDateTime, NaiveTime, Timelike, Utc,
};
use std::str::FromStr;

use crate::entities::errors::TimeError;
use crate::proto::common::ITimeUI;
use crate::utils::time::utc_to_tz;

const DAY_KEYS: &[(&str, i64)] = &[
    ("сегодня", 0),
    ("today", 0),
    ("завтра", 1),
    ("tomorrow", 1),
    ("послезавтра", 2),
];
const SECONDS_TO_COUNT_DOWN: i64 = 5 * 60 * 60;

pub struct TimeUI;

impl TimeUI {
    fn parse_dt_date<'a, 'b>(
        tokens: &'a [&'b str],
        default: NaiveDate,
    ) -> Result<(NaiveDate, &'a [&'b str]), TimeError> {
        if tokens.is_empty() {
            return Ok((default, tokens));
        }
        let token = tokens[0];
        let rest = &tokens[1..];
        for (key, shift) in DAY_KEYS {
            if *key == token {
                let date = default + Duration::days(*shift);
                return Ok((date, rest));
            }
        }
        let split = token
            .split('-')
            .map(|s| u32::from_str(s.trim()))
            .collect::<Vec<_>>();
        let (y, m, d) = match split[..] {
            [Ok(y), Ok(m), Ok(d)] => (y as i32, m, d),
            [Ok(m), Ok(d)] => (default.year(), m, d),
            [Ok(d)] => (default.year(), default.month(), d),
            _ => return Ok((default, tokens)),
        };
        match NaiveDate::from_ymd_opt(y, m, d) {
            None => Err(TimeError::InvalidDate),
            Some(val) => Ok((val, rest)),
        }
    }

    fn parse_dt_time<'a, 'b>(
        tokens: &'a [&'b str],
    ) -> Result<(NaiveTime, &'a [&'b str]), TimeError> {
        if tokens.is_empty() {
            return Err(TimeError::TimeNotSet);
        }
        let nums = tokens[0]
            .split(':')
            .map(|s| u32::from_str(s.trim()))
            .collect::<Vec<_>>();
        let (h, m, s) = match nums[..] {
            [Ok(h), Ok(m)] => (h, m, 0),
            _ => return Err(TimeError::InvalidTime),
        };
        match NaiveTime::from_hms_opt(h, m, s) {
            None => Err(TimeError::InvalidTime),
            Some(val) => Ok((val, &tokens[1..])),
        }
    }

    fn parse_tz(tokens: &[&str], default: &str) -> Result<String, TimeError> {
        if tokens.is_empty() {
            Ok(default.to_string())
        } else {
            Ok(tokens[0].to_string())
        }
    }

    fn count_down_minutes(duration: Duration) -> u64 {
        duration.num_minutes().unsigned_abs() % 60
    }

    fn show_count_down(duration: Duration) -> String {
        if duration < Duration::minutes(1) {
            return "Now".to_string();
        }
        let mut result = "After".to_string();
        if duration > Duration::hours(1) {
            let msg = format!(" {}h", duration.num_hours());
            result.push_str(&msg);
        }
        let msg = format!(" {}m", Self::count_down_minutes(duration));
        result.push_str(&msg);
        result
    }

    fn show_time_in_tz(time: DateTime<Utc>, tz: FixedOffset) -> String {
        let localized = utc_to_tz(time, tz);
        format!(
            "{}-{:02}-{:02} {:02}:{:02}",
            localized.year(),
            localized.month(),
            localized.day(),
            localized.hour(),
            localized.minute()
        )
    }
}

impl ITimeUI for TimeUI {
    fn parse_time(
        &self,
        text: &str,
        default_date: NaiveDate,
        default_code: &str,
    ) -> Result<(NaiveDateTime, String), TimeError> {
        let text = text.to_lowercase();
        let tokens = text
            .split(' ')
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>();
        let (date, rest) = Self::parse_dt_date(&tokens, default_date)?;
        let (time, rest) = Self::parse_dt_time(rest)?;
        let tz = Self::parse_tz(rest, default_code)?;
        Ok((NaiveDateTime::new(date, time), tz))
    }

    fn show_time(
        &self,
        time: DateTime<Utc>,
        tz_list: &[(&str, FixedOffset)],
        now: DateTime<Utc>,
    ) -> String {
        let delta = time - now;
        if delta.num_seconds() < SECONDS_TO_COUNT_DOWN {
            return Self::show_count_down(delta);
        }
        let mut result = Vec::new();
        for (tz_name, tz) in tz_list {
            let tz_time = Self::show_time_in_tz(time, *tz);
            result.push(format!("{}: {}", tz_name, tz_time));
        }
        result.join("\n")
    }
}
