use std::thread;
use std::time::Duration;

use chrono::Utc;
use telegram_bot_ars::{Api, ChatId, SendMessage};

use crate::entities::config::Config;
use crate::entities::errors::RobotError;
use crate::entities::events::{Event, EventWithSeconds};
use crate::entities::settings::Settings;
use crate::proto::common::ITimeUI;
use crate::proto::events::IEventRepo;
use crate::proto::robot::IRobot;
use crate::proto::settings::ISettingsRepo;

pub struct RobotNotify<'a, 'b, 'c, T: ITimeUI> {
    pub config: &'a Config,
    pub events_repo: &'b dyn IEventRepo,
    pub settings_repo: &'c dyn ISettingsRepo,
    pub time_ui: T,
}

impl<'a, 'b, 'c, T: ITimeUI> RobotNotify<'a, 'b, 'c, T> {
    #[inline]
    fn mark_as_sent(&self, notif: EventWithSeconds) -> Result<(), RobotError> {
        self.events_repo
            .set_send_notifications(notif.event.id, &notif.seconds_before)?;
        Ok(())
    }

    async fn send_batch_async(
        &self,
        notifs: &mut Vec<EventWithSeconds>,
        settings: &Settings,
    ) -> Vec<EventWithSeconds> {
        let api = Api::new(&self.config.token);
        let mut sent = Vec::new();
        while !notifs.is_empty() {
            if let Err(err) = self.send_one(&api, &notifs[0], settings).await {
                eprintln!("Notif error: {:?}", err);
                break;
            }
            sent.push(notifs.remove(0));
        }
        sent
    }

    async fn send_one(
        &self,
        api: &Api,
        notif: &EventWithSeconds,
        settings: &Settings,
    ) -> Result<(), telegram_bot_ars::Error> {
        let msg = self.prepare_text(&notif.event, settings);
        let chat_id: ChatId = notif.event.new.chat_id.into();
        api.send(SendMessage::new(chat_id, msg)).await?;
        Ok(())
    }

    fn prepare_text(&self, notif: &Event, settings: &Settings) -> String {
        let mut result = notif.new.name.clone();
        let now = Utc::now();
        let tz_list = settings.get_display_timezones();
        result.push('\n');
        result.push_str(&self.time_ui.show_time(notif.new.time, &tz_list, now));
        result.push('\n');
        result.push_str(&notif.new.message);
        result
    }

    fn send_notifications(
        &self,
        mut notifs: Vec<EventWithSeconds>,
    ) -> Result<Vec<EventWithSeconds>, RobotError> {
        let settings = self.settings_repo.get()?;
        let mut sent = Vec::new();
        for _ in 0..self.config.max_tg_retries {
            let mut res = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(self.send_batch_async(&mut notifs, &settings));
            sent.append(&mut res);
            if notifs.is_empty() {
                break;
            }
            let dur = Duration::from_secs_f64(self.config.notifier_send_sleep);
            thread::sleep(dur);
        }
        Ok(sent)
    }
}

impl<'a, 'b, 'c, T: ITimeUI> IRobot for RobotNotify<'a, 'b, 'c, T> {
    fn name(&self) -> &str {
        "notify"
    }

    fn run(&mut self) -> Result<(), RobotError> {
        loop {
            let notifs = self.events_repo.get_events_to_notify()?;
            let sent = self.send_notifications(notifs)?;
            for notif in sent {
                self.mark_as_sent(notif)?;
            }
            self.events_repo.remove_old_events()?;
            let dur = Duration::from_secs_f64(self.config.notifier_check_sleep);
            thread::sleep(dur);
        }
    }
}
