use std::collections::HashSet;

use crate::app::config::config;
use crate::app::tg_use_cases;
use crate::impls::robot_ui::RobotUI;
use crate::proto::robot::ITgUseCase;

fn make_robot() -> RobotUI<'static, 'static, 'static> {
    RobotUI {
        config: config(),
        use_cases: tg_use_cases::all(),
    }
}

const CASES: &[(&str, &str)] = &[
    ("/help", "/help"),
    ("/help ...", "/help"),
    ("/addstreamer serje,xxx\nlink", "/addstreamer"),
    ("/check", "/check"),
    ("/addevent stream\ntodat 11:40", "/addevent"),
    ("/rmevent 123", "/rmevent"),
    ("/rmstreamer abc", "/rmstreamer"),
    ("/rmtz msk", "/rmtz"),
    ("/settz msk +3", "/settz"),
    ("/events", "/events"),
    ("/streamers abc", "/streamers"),
    ("/streamers", "/streamers"),
    ("/streamer abc", "/streamer"),
];

#[test]
fn test_unique() {
    let cases = tg_use_cases::all();
    let unique = cases
        .iter()
        .map(|case| case.command())
        .collect::<HashSet<_>>();
    assert_eq!(unique.len(), cases.len());
}

#[test]
fn test_dispatch() {
    let robot = make_robot();
    for (request, cmd) in CASES {
        let uc = match robot.get_use_case(request) {
            None => panic!("request({:?}): no use case matched", request),
            Some(val) => val,
        };
        if uc.command() != *cmd {
            panic!(
                "request({:?}): matched as {}, not as {}",
                request,
                uc.command(),
                cmd
            )
        }
    }
}
