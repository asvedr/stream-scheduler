use easy_sqlite::errors::DbResult;
use easy_sqlite::traits::repo::{IConnection, IDbRepo, INewDbRepo};
use easy_sqlite::traits::table::ITable;
use easy_sqlite::{deescape, escape, SStr, SVec, TableManager};

use crate::entities::errors::StreamerRepoError;
use crate::entities::streamers::{NewStreamer, Streamer};
use crate::proto::streamers::IStreamersRepo;

pub struct StreamersRepo<Cnn: IConnection> {
    streamers: TableManager<Cnn, StreamerTable>,
    tags: TableManager<Cnn, TagTable>,
    streamers_insert: String,
    streamers_select: String,
    tags_insert: String,
}

pub struct StreamerTable;
pub struct TagTable;

impl ITable for StreamerTable {
    const PK: SStr = "id";
    const NAME: SStr = "streamers";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("name", "STRING NOT NULL"),
        ("chat", "INTEGER NOT NULL"),
        ("links", "STRING NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &["name", "chat"];
    const UNIQUE: SVec<SStr> = &["name,chat"];
}

impl ITable for TagTable {
    const PK: SStr = "id";
    const NAME: SStr = "tags";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("streamer_id", "INTEGER NOT NULL"),
        ("text", "STRING NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &["streamer_id", "text"];
    const UNIQUE: SVec<SStr> = &["streamer_id,text"];
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> =
        &[("streamer_id", StreamerTable::NAME, StreamerTable::PK)];
}

impl<Cnn: IConnection + Clone> StreamersRepo<Cnn> {
    pub fn new(cnn: Cnn) -> Self {
        Self {
            streamers: TableManager::create(cnn.clone()),
            tags: TableManager::create(cnn),
            streamers_insert: StreamerTable::get_columns_as_str(&[StreamerTable::PK]),
            streamers_select: StreamerTable::get_columns_as_str(&[]),
            tags_insert: TagTable::get_columns_as_str(&[TagTable::PK]),
        }
    }
}

impl<Cnn: IConnection> StreamersRepo<Cnn> {
    fn serialize_streamer(row: &rusqlite::Row) -> rusqlite::Result<Streamer> {
        let links = deescape(row.get(3)?)
            .split('\n')
            .map(|s| s.to_string())
            .collect::<Vec<_>>();
        Ok(Streamer {
            id: row.get(0)?,
            new: NewStreamer {
                name: deescape(row.get(1)?),
                chat: row.get(2)?,
                tags: vec![],
                links,
            },
        })
    }
}

impl<Cnn: IConnection> IDbRepo for StreamersRepo<Cnn> {
    fn init(&self) -> DbResult<()> {
        self.streamers.init()?;
        self.tags.init()
    }
    fn drop(&self) -> DbResult<()> {
        self.tags.drop()?;
        self.streamers.drop()
    }
    fn set_indexes(&self) -> DbResult<()> {
        self.streamers.set_indexes()?;
        self.tags.set_indexes()
    }
    fn drop_indexes(&self) -> DbResult<()> {
        self.streamers.drop_indexes()?;
        self.tags.drop_indexes()
    }
    fn get_size(&self) -> DbResult<usize> {
        self.streamers.get_size()
    }
}

impl<Cnn: IConnection> IStreamersRepo for StreamersRepo<Cnn> {
    fn register(&self, streamer: NewStreamer) -> Result<i64, StreamerRepoError> {
        let links = streamer.links.join("\n");
        let query = format!(
            "INSERT INTO {tbl} ({columns}) VALUES ({name}, {chat}, {links})",
            tbl = StreamerTable::NAME,
            columns = self.streamers_insert,
            name = escape(&streamer.name),
            chat = streamer.chat,
            links = escape(&links),
        );
        let streamer_id = self.streamers.execute_return_id(&query)?;
        if streamer.tags.is_empty() {
            return Ok(streamer_id);
        }
        let values = streamer
            .tags
            .iter()
            .map(|t| format!("({}, {})", streamer_id, escape(t)))
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            "INSERT INTO {tbl} ({columns}) VALUES {values}",
            tbl = TagTable::NAME,
            columns = self.tags_insert,
            values = values,
        );
        self.tags.execute(&query)?;
        Ok(streamer_id)
    }

    fn remove(&self, streamer_id: i64) -> Result<(), StreamerRepoError> {
        let query = format!(
            "DELETE FROM {tbl} WHERE streamer_id = {id}",
            tbl = TagTable::NAME,
            id = streamer_id
        );
        self.tags.execute(&query)?;
        let query = format!(
            "DELETE FROM {tbl} WHERE id = {id}",
            tbl = StreamerTable::NAME,
            id = streamer_id
        );
        self.streamers.execute(&query)?;
        Ok(())
    }

    fn get_by_id(&self, id: i64) -> Result<Streamer, StreamerRepoError> {
        let query = format!(
            "SELECT {columns} FROM {tbl} WHERE id = {id}",
            columns = self.streamers_select,
            tbl = StreamerTable::NAME,
            id = id,
        );
        let mut streamer = self.streamers.get_one(&query, Self::serialize_streamer)?;
        let query = format!(
            "SELECT text FROM {tbl} WHERE streamer_id = {id} ORDER BY text",
            tbl = TagTable::NAME,
            id = streamer.id
        );
        streamer.new.tags = self.tags.get_many(&query, |row| row.get(0))?;
        Ok(streamer)
    }

    fn get_by_name(&self, chat: i64, name: &str) -> Result<Streamer, StreamerRepoError> {
        let query = format!(
            "SELECT {columns} FROM {tbl} WHERE chat = {chat} AND name = {name}",
            columns = self.streamers_select,
            tbl = StreamerTable::NAME,
            chat = chat,
            name = escape(name)
        );
        let mut streamer = self.streamers.get_one(&query, Self::serialize_streamer)?;
        let query = format!(
            "SELECT text FROM {tbl} WHERE streamer_id = {id} ORDER BY text",
            tbl = TagTable::NAME,
            id = streamer.id
        );
        streamer.new.tags = self.tags.get_many(&query, |row| row.get(0))?;
        Ok(streamer)
    }

    fn get_all_for_chat(&self, chat: i64) -> Result<Vec<Streamer>, StreamerRepoError> {
        let query = format!(
            "SELECT {columns} FROM {tbl} WHERE chat = {chat} ORDER BY id",
            columns = self.streamers_select,
            tbl = StreamerTable::NAME,
            chat = chat,
        );
        let streamers = self.streamers.get_many(&query, Self::serialize_streamer)?;
        Ok(streamers)
    }

    fn get_all(&self) -> Result<Vec<Streamer>, StreamerRepoError> {
        let query = format!(
            "SELECT {columns} FROM {tbl} ORDER BY id",
            columns = self.streamers_select,
            tbl = StreamerTable::NAME,
        );
        let streamers = self.streamers.get_many(&query, Self::serialize_streamer)?;
        Ok(streamers)
    }

    fn get_by_tag(
        &self,
        opt_chat: Option<i64>,
        text: &str,
    ) -> Result<Vec<Streamer>, StreamerRepoError> {
        let chat_case = if let Some(chat) = opt_chat {
            format!("s.chat = {} AND ", chat)
        } else {
            "".to_string()
        };
        let query = format!(
            r#"
                SELECT DISTINCT t.streamer_id
                FROM {tags} as t JOIN {streamers} as s
                    ON t.streamer_id = s.id
                WHERE {chat_case} t.text = {text} ORDER BY t.streamer_id
            "#,
            tags = TagTable::NAME,
            streamers = StreamerTable::NAME,
            chat_case = chat_case,
            text = escape(text),
        );
        let id_list: Vec<i64> = self.tags.get_many(&query, |row| row.get(0))?;
        if id_list.is_empty() {
            return Ok(Vec::new());
        }
        let id_list = id_list
            .iter()
            .map(|val| val.to_string())
            .collect::<Vec<_>>()
            .join(",");
        let query = format!(
            "SELECT {columns} FROM {tbl} WHERE id IN ({id_list})",
            columns = self.streamers_select,
            tbl = StreamerTable::NAME,
            id_list = id_list,
        );
        let streamers = self.streamers.get_many(&query, Self::serialize_streamer)?;
        Ok(streamers)
    }
}
