use crate::proto::common::ITimeGetter;
use chrono::{DateTime, Utc};

pub struct TimeGetter;

impl ITimeGetter for TimeGetter {
    fn now(&self) -> DateTime<Utc> {
        Utc::now()
    }
}
