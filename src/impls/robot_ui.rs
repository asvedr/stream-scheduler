use telegram_bot_ars::{AllowedUpdate, Api, MessageKind, SendMessage, UpdateKind};

use futures::StreamExt;

use crate::entities::config::Config;
use crate::entities::errors::RobotError;
use crate::proto::robot::{IRobot, ITgUseCase};

pub struct RobotUI<'a, 'b, 'c> {
    pub config: &'a Config,
    pub use_cases: &'b [&'c dyn ITgUseCase],
}

impl<'a, 'b, 'c> RobotUI<'a, 'b, 'c> {
    pub fn get_use_case(&self, cmd: &str) -> Option<&'c dyn ITgUseCase> {
        for uc in self.use_cases.iter() {
            if cmd.starts_with(uc.command()) {
                return Some(*uc);
            }
        }
        None
    }

    async fn arun(&mut self) -> Result<(), RobotError> {
        let api = Api::new(&self.config.token);
        let mut stream = api.stream();
        stream.allowed_updates(&[AllowedUpdate::Message]);
        while let Some(update) = stream.next().await {
            // If the received update contains a new message...
            let update = match update {
                Ok(val) => val,
                Err(err) => {
                    eprintln!("Invalid tg update: {:?}", err);
                    continue;
                }
            };
            let message = match update.kind {
                Some(UpdateKind::Message(val)) => val,
                _ => continue,
            };
            let chat_id = message.chat.id();
            let text = match message.kind {
                MessageKind::Text { ref data, .. } => data,
                _ => continue,
            };
            let uc = match self.get_use_case(text) {
                Some(val) => val,
                None => continue,
            };
            let msg = match uc.execute(chat_id.into(), text) {
                Ok(resp) => resp,
                Err(err) => format!("ERROR: {:?}", err),
            };
            api.send(SendMessage::new(chat_id, msg)).await?;
        }
        Ok(())
    }
}

impl<'a, 'b, 'c> IRobot for RobotUI<'a, 'b, 'c> {
    fn name(&self) -> &str {
        "ui"
    }

    fn run(&mut self) -> Result<(), RobotError> {
        tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(self.arun())
    }
}
