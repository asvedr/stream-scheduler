use mddd::macros::singleton_mutex;
use mddd::traits::IConfig;

use crate::entities::config::Config;

singleton_mutex!(
    pub fn config() -> Config {
        match Config::build() {
            Ok(val) => val,
            Err(err) => panic!("Config error: {:?}", err),
        }
    }
);
