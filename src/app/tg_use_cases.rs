use mddd::macros::{singleton_mutex, singleton_simple};

use crate::app::repos;
use crate::impls::time_getter::TimeGetter;
use crate::impls::time_ui::TimeUI;
use crate::proto::robot::ITgUseCase;
use crate::tg_use_cases::add_streamer::AddStreamerUC;
use crate::tg_use_cases::check_time::CheckTimeUC;
use crate::tg_use_cases::create_event::CreateEventUC;
use crate::tg_use_cases::help::HelpUC;
use crate::tg_use_cases::remove_event::RemoveEventUC;
use crate::tg_use_cases::remove_streamer::RemoveStreamerUC;
use crate::tg_use_cases::remove_tz::RemoveTzUC;
use crate::tg_use_cases::set_tz::SetTzUC;
use crate::tg_use_cases::show_events::ShowEventsUC;
use crate::tg_use_cases::show_streamer::ShowStreamerUC;
use crate::tg_use_cases::show_streamers::ShowStreamersUC;

singleton_simple!(
    fn help() -> HelpUC {
        HelpUC {
            command_factory: &all,
        }
    }
);

singleton_simple!(
    fn add_streamer() -> AddStreamerUC<'static> {
        AddStreamerUC {
            streamers_repo: repos::streamers(),
        }
    }
);

singleton_simple!(
    fn show_streamer() -> ShowStreamerUC<'static> {
        ShowStreamerUC {
            streamers_repo: repos::streamers(),
        }
    }
);

singleton_simple!(
    fn show_streamers() -> ShowStreamersUC<'static> {
        ShowStreamersUC {
            streamers_repo: repos::streamers(),
        }
    }
);

singleton_simple!(
    fn remove_streamer() -> RemoveStreamerUC<'static> {
        RemoveStreamerUC {
            streamers_repo: repos::streamers(),
        }
    }
);

singleton_simple!(
    fn check_time() -> CheckTimeUC<'static> {
        CheckTimeUC {
            settings_repo: repos::settings(),
        }
    }
);

singleton_simple!(
    fn set_tz() -> SetTzUC<'static> {
        SetTzUC {
            settings_repo: repos::settings(),
        }
    }
);

singleton_simple!(
    fn remove_tz() -> RemoveTzUC<'static> {
        RemoveTzUC {
            settings_repo: repos::settings(),
        }
    }
);

singleton_simple!(
    fn create_event() -> CreateEventUC<'static, 'static, TimeUI, TimeGetter> {
        CreateEventUC {
            settings_repo: repos::settings(),
            events_repo: repos::events(),
            time_ui: TimeUI,
            time_getter: TimeGetter,
        }
    }
);

singleton_simple!(
    fn remove_event() -> RemoveEventUC<'static> {
        RemoveEventUC {
            events_repo: repos::events(),
        }
    }
);

singleton_simple!(
    fn show_events() -> ShowEventsUC<'static, 'static, TimeUI, TimeGetter> {
        ShowEventsUC {
            events_repo: repos::events(),
            settings_repo: repos::settings(),
            time_ui: TimeUI,
            time_getter: TimeGetter,
        }
    }
);

singleton_mutex!(
    pub fn all() -> Vec<&'static dyn ITgUseCase> {
        vec![
            help() as &'static dyn ITgUseCase,
            add_streamer() as &'static dyn ITgUseCase,
            show_streamers() as &'static dyn ITgUseCase,
            show_streamer() as &'static dyn ITgUseCase,
            remove_streamer() as &'static dyn ITgUseCase,
            create_event() as &'static dyn ITgUseCase,
            remove_event() as &'static dyn ITgUseCase,
            show_events() as &'static dyn ITgUseCase,
            check_time() as &'static dyn ITgUseCase,
            set_tz() as &'static dyn ITgUseCase,
            remove_tz() as &'static dyn ITgUseCase,
        ]
    }
);
