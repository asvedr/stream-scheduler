use crate::app::repos::{events, settings, streamers};
use crate::cli_use_cases::add_event::AddEventUC;
use crate::cli_use_cases::add_streamer::AddStreamerUC;
use crate::cli_use_cases::config_help::ConfigHelpUC;
use crate::cli_use_cases::get_events::GetEventsUC;
use crate::cli_use_cases::get_settings::GetSettingsUC;
use crate::cli_use_cases::get_streamers::GetStreamerUC;
use crate::cli_use_cases::rm_event::RmEventUC;
use crate::cli_use_cases::rm_old_events::RmOldEventsUC;
use crate::cli_use_cases::rm_streamer::RmStreamerUC;
use crate::cli_use_cases::run::RunUC;
use crate::cli_use_cases::upd_settings::UpdSettingsUC;
use crate::impls::time_getter::TimeGetter;
use crate::impls::time_ui::TimeUI;

pub fn run() -> RunUC {
    RunUC
}

pub fn config_help() -> ConfigHelpUC {
    ConfigHelpUC
}

pub fn get_settings() -> GetSettingsUC<'static> {
    GetSettingsUC {
        settings_repo: settings(),
    }
}

pub fn upd_settings() -> UpdSettingsUC<'static> {
    UpdSettingsUC {
        settings_repo: settings(),
    }
}

pub fn get_streamers() -> GetStreamerUC<'static> {
    GetStreamerUC {
        streamers_repo: streamers(),
    }
}

pub fn add_streamer() -> AddStreamerUC<'static> {
    AddStreamerUC {
        streamers_repo: streamers(),
    }
}

pub fn rm_streamer() -> RmStreamerUC<'static> {
    RmStreamerUC {
        streamers_repo: streamers(),
    }
}

pub fn get_events() -> GetEventsUC<'static, 'static, TimeGetter, TimeUI> {
    GetEventsUC {
        settings_repo: settings(),
        events_repo: events(),
        time_getter: TimeGetter,
        time_ui: TimeUI,
    }
}

pub fn add_event() -> AddEventUC<'static, 'static, TimeGetter, TimeUI> {
    AddEventUC {
        settings_repo: settings(),
        events_repo: events(),
        time_getter: TimeGetter,
        time_ui: TimeUI,
    }
}

pub fn rm_event() -> RmEventUC<'static> {
    RmEventUC {
        events_repo: events(),
    }
}

pub fn rm_old_events() -> RmOldEventsUC<'static> {
    RmOldEventsUC {
        events_repo: events(),
    }
}
