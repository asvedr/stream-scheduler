use easy_sqlite::traits::repo::IDbRepo;
use easy_sqlite::MSQLConnection;
use mddd::macros::singleton_simple;

use crate::app::config::config;
use crate::impls::events_repo::EventRepo;
use crate::impls::settings_repo::SettingsRepo;
use crate::impls::streamers_repo::StreamersRepo;

singleton_simple!(
    fn sqlite_connect() -> MSQLConnection {
        let cnf = config();
        match MSQLConnection::new(&cnf.db) {
            Ok(val) => val,
            Err(err) => panic!("Can not create database: {:?}", err),
        }
    }
);

singleton_simple!(
    pub fn events() -> EventRepo<&'static MSQLConnection> {
        EventRepo::new(sqlite_connect())
    }
);

singleton_simple!(
    pub fn streamers() -> StreamersRepo<&'static MSQLConnection> {
        StreamersRepo::new(sqlite_connect())
    }
);

singleton_simple!(
    pub fn settings() -> SettingsRepo<&'static MSQLConnection> {
        SettingsRepo::new(sqlite_connect())
    }
);

pub fn all() -> Vec<(&'static str, &'static dyn IDbRepo)> {
    vec![
        ("events", events()),
        ("streamers", streamers()),
        ("settings", settings()),
    ]
}
