use crate::app::config::config;
use crate::app::repos;
use crate::app::tg_use_cases;
use crate::impls::robot_notify::RobotNotify;
use crate::impls::robot_ui::RobotUI;
use crate::impls::time_ui::TimeUI;

pub fn ui() -> RobotUI<'static, 'static, 'static> {
    RobotUI {
        config: config(),
        use_cases: tg_use_cases::all(),
    }
}

pub fn notify() -> RobotNotify<'static, 'static, 'static, TimeUI> {
    RobotNotify {
        config: config(),
        events_repo: repos::events(),
        settings_repo: repos::settings(),
        time_ui: TimeUI,
    }
}
