use easy_sqlite::traits::repo::IDbRepo;
use std::thread;
use std::time::Duration;

use crate::app::config::config;
use crate::proto::robot::IRobot;

#[derive(Default)]
pub struct Launcher {
    handlers: Vec<thread::JoinHandle<()>>,
}

struct Wrapper<Robot> {
    robot: Robot,
}

unsafe impl<T> Sync for Wrapper<T> {}
unsafe impl<T> Send for Wrapper<T> {}

impl<R: IRobot> Wrapper<R> {
    fn run(mut self) {
        let timeout = config().restart_timeout;
        loop {
            if let Err(err) = self.robot.run() {
                eprintln!("ERR robot {} failed with: {:?}", self.robot.name(), err);
                thread::sleep(Duration::from_secs(timeout))
            } else {
                return;
            }
        }
    }
}

impl Launcher {
    pub fn init_repos(self, repos: Vec<(&str, &'static dyn IDbRepo)>) -> Self {
        for (name, repo) in repos {
            if let Err(err) = repo.init() {
                panic!("Can not init repo {}: {:?}", name, err)
            }
        }
        self
    }

    pub fn add_robot<R: IRobot + 'static>(mut self, robot: R) -> Self {
        let wrapper = Wrapper { robot };
        let handler = thread::spawn(move || wrapper.run());
        self.handlers.push(handler);
        self
    }

    pub fn wait(self) {
        for handler in self.handlers {
            if let Err(err) = handler.join() {
                eprintln!("thread failed: {:?}", err)
            }
        }
    }
}
