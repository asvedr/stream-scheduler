use crate::entities::errors::TgUseCaseError;
use crate::entities::events::Event;
use crate::entities::settings::Settings;
use crate::proto::common::{ITimeGetter, ITimeUI};
use crate::proto::events::IEventRepo;
use crate::proto::robot::ITgUseCase;
use crate::proto::settings::ISettingsRepo;

pub struct ShowEventsUC<'a, 'b, TU: ITimeUI, TG: ITimeGetter> {
    pub events_repo: &'a dyn IEventRepo,
    pub settings_repo: &'b dyn ISettingsRepo,
    pub time_ui: TU,
    pub time_getter: TG,
}

impl<'a, 'b, TU: ITimeUI, TG: ITimeGetter> ShowEventsUC<'a, 'b, TU, TG> {
    fn show_event(&self, event: &Event, settings: &Settings) -> String {
        let mut rows = vec![format!("[id={}] {}", event.id, event.new.name)];
        let zones = settings.get_display_timezones();
        let now = self.time_getter.now();
        rows.push(self.time_ui.show_time(event.new.time, &zones, now));
        rows.push(event.new.message.clone());
        rows.join("\n")
    }
}

impl<'a, 'b, TU: ITimeUI, TG: ITimeGetter> ITgUseCase for ShowEventsUC<'a, 'b, TU, TG> {
    fn command(&self) -> &str {
        "/events"
    }

    fn help(&self) -> &str {
        ": показать все запланированные стримы в этом чате"
    }

    fn execute(&self, chat: i64, _: &str) -> Result<String, TgUseCaseError> {
        let now = self.time_getter.now();
        let events = self
            .events_repo
            .get_chat_events(chat, true)?
            .into_iter()
            .filter(|ev| ev.new.time >= now)
            .collect::<Vec<_>>();
        if events.is_empty() {
            return Ok("No events".to_string());
        }
        let settings = self.settings_repo.get()?;
        let msg = events
            .into_iter()
            .map(|ev| self.show_event(&ev, &settings))
            .collect::<Vec<_>>()
            .join("\n\n");
        Ok(msg)
    }
}
