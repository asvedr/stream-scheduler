use std::str::FromStr;

use crate::entities::errors::TgUseCaseError;
use crate::proto::robot::ITgUseCase;
use crate::proto::settings::ISettingsRepo;

pub struct SetTzUC<'a> {
    pub settings_repo: &'a dyn ISettingsRepo,
}

impl<'a> ITgUseCase for SetTzUC<'a> {
    fn command(&self) -> &str {
        "/settz"
    }

    fn help(&self) -> &str {
        "<code> hour-shift: добавить/обновить временную зону"
    }

    fn execute(&self, _: i64, text: &str) -> Result<String, TgUseCaseError> {
        let stripped = text[self.command().len()..].trim();
        let tokens = stripped
            .split(' ')
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>();
        if tokens.len() != 2 {
            return Ok("Invalid arguments".to_string());
        }
        let shift = match i32::from_str(tokens[1]) {
            Ok(val) => val,
            Err(_) => return Ok("Shift is not number".to_string()),
        };
        if shift.abs() >= 24 {
            return Ok("Shift must be in [-23..23]".to_string());
        }
        let mut settings = self.settings_repo.get()?;
        settings.update_tz(tokens[0], shift);
        self.settings_repo.update(settings)?;
        Ok("Ok".to_string())
    }
}
