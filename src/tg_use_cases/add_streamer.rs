use crate::entities::errors::TgUseCaseError;
use crate::entities::streamers::NewStreamer;
use crate::proto::robot::ITgUseCase;
use crate::proto::streamers::IStreamersRepo;

pub struct AddStreamerUC<'a> {
    pub streamers_repo: &'a dyn IStreamersRepo,
}

impl<'a> AddStreamerUC<'a> {
    fn process_header(&self, text: &str) -> Option<(String, Vec<String>)> {
        let prefix_len = self.command().len();
        let mut tokens = text[prefix_len..]
            .split(',')
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| s.to_lowercase())
            .collect::<Vec<_>>();
        if tokens.is_empty() {
            return None;
        }
        let name = tokens.remove(0);
        Some((name, tokens))
    }
}

impl<'a> ITgUseCase for AddStreamerUC<'a> {
    fn command(&self) -> &str {
        "/addstreamer"
    }

    fn help(&self) -> &str {
        "name, tag1, tag2, ...<enter> links: добавить нового стримера в чат"
    }

    fn execute(&self, chat: i64, text: &str) -> Result<String, TgUseCaseError> {
        let lines = text.split('\n').collect::<Vec<_>>();
        let (name, tags) = match self.process_header(lines[0]) {
            Some(val) => val,
            _ => return Ok("Name not set".to_string()),
        };
        let links = lines[1..].iter().map(|s| s.to_string()).collect();
        let streamer = NewStreamer {
            name,
            chat,
            tags,
            links,
        };
        self.streamers_repo.register(streamer)?;
        Ok("Ok".to_string())
    }
}
