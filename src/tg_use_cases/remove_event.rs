use std::str::FromStr;

use crate::entities::errors::{EventRepoError, TgUseCaseError};
use crate::proto::events::IEventRepo;
use crate::proto::robot::ITgUseCase;

pub struct RemoveEventUC<'a> {
    pub events_repo: &'a dyn IEventRepo,
}

impl<'a> ITgUseCase for RemoveEventUC<'a> {
    fn command(&self) -> &str {
        "/rmevent"
    }

    fn help(&self) -> &str {
        "<id>: удалить запланированный стрим по id"
    }

    fn execute(&self, chat: i64, text: &str) -> Result<String, TgUseCaseError> {
        let ids = text[self.command().len()..].trim();
        if ids.is_empty() {
            return Ok("Id not set".to_string());
        }
        let id = match i64::from_str(ids) {
            Ok(val) => val,
            Err(_) => return Ok("Invalid id".to_string()),
        };
        let event = match self.events_repo.get_by_id(id) {
            Ok(val) => val,
            Err(EventRepoError::NotFound) => return Ok("Event not found".to_string()),
            Err(err) => return Err(err.into()),
        };
        if event.new.chat_id != chat {
            return Ok("Event not found".to_string());
        }
        self.events_repo.remove_event(id)?;
        Ok("Ok".to_string())
    }
}
