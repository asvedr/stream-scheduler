/*
   /check time msk
   - показать время в часовом поясе msk

   позволяет понять насколько правильно настроены пояса
*/
use chrono::{Timelike, Utc};

use crate::entities::errors::TgUseCaseError;
use crate::entities::settings::Settings;
use crate::proto::robot::ITgUseCase;
use crate::proto::settings::ISettingsRepo;
use crate::utils::time::utc_to_tz;

pub struct CheckTimeUC<'a> {
    pub settings_repo: &'a dyn ISettingsRepo,
}

impl<'a> CheckTimeUC<'a> {
    fn get_time_for_code(&self, code: &str, settings: &Settings) -> Result<String, TgUseCaseError> {
        let tz = match settings.get_tz(code) {
            None => return Ok("invalid timezone".to_string()),
            Some(val) => val,
        };
        let now = utc_to_tz(Utc::now(), tz);
        let msg = format!("{:02}:{:02}:{:02}", now.hour(), now.minute(), now.second());
        Ok(msg)
    }
}

impl<'a> ITgUseCase for CheckTimeUC<'a> {
    fn command(&self) -> &str {
        "/check"
    }

    fn help(&self) -> &str {
        "[tz]: показать время в зоне tz"
    }

    fn execute(&self, _: i64, text: &str) -> Result<String, TgUseCaseError> {
        let code = text[self.command().len()..].trim();
        let settings = self.settings_repo.get()?;
        if !code.is_empty() {
            return self.get_time_for_code(code, &settings);
        }
        let msg = settings
            .timezones
            .iter()
            .map(|(tz, _)| -> Result<String, TgUseCaseError> {
                let time = self.get_time_for_code(tz, &settings)?;
                Ok(format!("{}: {}", tz, time))
            })
            .collect::<Result<Vec<_>, _>>()?
            .join("\n");
        Ok(msg)
    }
}
