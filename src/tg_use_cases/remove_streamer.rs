use crate::entities::errors::{StreamerRepoError, TgUseCaseError};
use crate::proto::robot::ITgUseCase;
use crate::proto::streamers::IStreamersRepo;

pub struct RemoveStreamerUC<'a> {
    pub streamers_repo: &'a dyn IStreamersRepo,
}

impl<'a> ITgUseCase for RemoveStreamerUC<'a> {
    fn command(&self) -> &str {
        "/rmstreamer"
    }

    fn help(&self) -> &str {
        "name: удалить стримера по имени"
    }

    fn execute(&self, chat: i64, text: &str) -> Result<String, TgUseCaseError> {
        let name = text[self.command().len()..].trim().to_lowercase();
        if name.is_empty() {
            return Ok("Name not set".to_string());
        }
        let id = match self.streamers_repo.get_by_name(chat, &name) {
            Ok(streamer) => streamer.id,
            Err(StreamerRepoError::NotFound) => return Ok("Not found".to_string()),
            Err(err) => return Err(err.into()),
        };
        self.streamers_repo.remove(id)?;
        Ok("Ok".to_string())
    }
}
