use crate::entities::errors::StreamerRepoError;
use crate::entities::streamers::{NewStreamer, Streamer};
use crate::factories::streamers_repo::MockStreamersRepo;
use crate::proto::robot::ITgUseCase;
use crate::proto::streamers::IStreamersRepo;
use crate::tg_use_cases::show_streamer::ShowStreamerUC;

fn make_repos() -> MockStreamersRepo {
    MockStreamersRepo::default()
}

fn make_uc(streamers_repo: &MockStreamersRepo) -> ShowStreamerUC {
    ShowStreamerUC { streamers_repo }
}

#[test]
fn test_found_by_name() {
    let repo = make_repos();
    repo.get_by_name.push_eq(
        (1, "nick".to_string()),
        Ok(Streamer {
            id: 0,
            new: NewStreamer {
                name: "nick".to_string(),
                chat: 0,
                tags: vec!["kek".to_string(), "lol".to_string()],
                links: vec!["x: y".to_string()],
            },
        }),
    );
    let uc = make_uc(&repo);
    assert_eq!(uc.execute(1, "/streamer Nick ").unwrap(), "Nick:\n  x: y")
}

#[test]
fn test_found_by_tag() {
    let repo = make_repos();
    repo.get_by_name
        .push_eq((1, "nick".to_string()), Err(StreamerRepoError::NotFound));
    repo.get_by_tag.push_eq(
        (Some(1), "nick".to_string()),
        Ok(vec![
            Streamer {
                id: 0,
                new: NewStreamer {
                    name: "lol".to_string(),
                    links: vec!["x: y".to_string()],
                    ..Default::default()
                },
            },
            Streamer {
                id: 1,
                new: NewStreamer {
                    name: "lel".to_string(),
                    ..Default::default()
                },
            },
        ]),
    );
    let uc = make_uc(&repo);
    assert_eq!(
        uc.execute(1, "/streamer Nick ").unwrap(),
        "Lol:\n  x: y\n\nLel:"
    )
}

#[test]
fn test_not_found() {
    let repo = make_repos();
    repo.get_by_name
        .push_eq((1, "nick".to_string()), Err(StreamerRepoError::NotFound));
    repo.get_by_tag
        .push_eq((Some(1), "nick".to_string()), Ok(Vec::new()));
    assert_eq!(
        make_uc(&repo).execute(1, "/streamer Nick").unwrap(),
        "Not found"
    )
}

#[test]
fn test_invalid_input() {
    let repo = make_repos();
    assert_eq!(
        make_uc(&repo).execute(1, "/streamer ").unwrap(),
        "Name not set"
    )
}
