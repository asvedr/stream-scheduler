use chrono::{TimeZone, Utc};

use crate::entities::events::{Event, NewEvent};
use crate::entities::settings::Settings;
use crate::factories::events_repo::MockEventRepo;
use crate::factories::settings_repo::MockSettingsRepo;
use crate::factories::time_getter::MockTimeGetter;
use crate::impls::time_ui::TimeUI;
use crate::proto::robot::ITgUseCase;
use crate::tg_use_cases::show_events::ShowEventsUC;

fn make_repos() -> (MockEventRepo, MockSettingsRepo) {
    let e_repo = MockEventRepo::default();
    let s_repo = MockSettingsRepo::default();
    s_repo.get.push_unchecked(Ok(Settings::default()));
    (e_repo, s_repo)
}

fn make_uc<'a, 'b>(
    events_repo: &'a MockEventRepo,
    settings_repo: &'b MockSettingsRepo,
) -> ShowEventsUC<'a, 'b, TimeUI, MockTimeGetter> {
    let now = Utc.with_ymd_and_hms(2020, 5, 9, 11, 30, 1).unwrap();
    ShowEventsUC {
        events_repo,
        settings_repo,
        time_ui: TimeUI,
        time_getter: MockTimeGetter { now },
    }
}

#[test]
fn test_empty() {
    let (e_repo, s_repo) = make_repos();
    e_repo.get_chat_events.push_eq((1, true), Ok(Vec::new()));
    let uc = make_uc(&e_repo, &s_repo);
    let cmd = uc.command();
    assert_eq!(uc.execute(1, cmd).unwrap(), "No events",)
}

#[test]
fn test_list() {
    let (e_repo, s_repo) = make_repos();
    let events = vec![
        Event {
            id: 1,
            new: NewEvent {
                name: "Aaa".to_string(),
                chat_id: 1,
                timezone: "msk".to_string(),
                time: Utc.with_ymd_and_hms(2020, 5, 10, 11, 30, 0).unwrap(),
                message: "xxx".to_string(),
            },
        },
        Event {
            id: 2,
            new: NewEvent {
                name: "Bbb".to_string(),
                chat_id: 1,
                timezone: "utc".to_string(),
                time: Utc.with_ymd_and_hms(2020, 5, 9, 11, 40, 0).unwrap(),
                message: "xxx".to_string(),
            },
        },
    ];
    e_repo.get_chat_events.push_eq((1, true), Ok(events));
    let uc = make_uc(&e_repo, &s_repo);
    let cmd = uc.command();
    assert_eq!(
        uc.execute(1, cmd).unwrap(),
        "[id=1] Aaa\nmsk: 2020-05-10 14:30\ncy: 2020-05-10 13:30\nxxx\n\n[id=2] Bbb\nAfter 9m\nxxx",
    )
}
