use crate::entities::streamers::NewStreamer;
use crate::factories::streamers_repo::MockStreamersRepo;
use crate::proto::robot::ITgUseCase;
use crate::tg_use_cases::add_streamer::AddStreamerUC;

fn make_repos() -> MockStreamersRepo {
    MockStreamersRepo::default()
}

fn make_uc(streamers_repo: &MockStreamersRepo) -> AddStreamerUC {
    AddStreamerUC { streamers_repo }
}

#[test]
fn test_empty_cmd() {
    let streamers_repo = make_repos();
    let uc = make_uc(&streamers_repo);
    assert_eq!(
        uc.execute(1, "/addstreamer "),
        Ok("Name not set".to_string())
    )
}

#[test]
fn test_only_name() {
    let streamers_repo = make_repos();
    streamers_repo.register.push_eq(
        NewStreamer {
            name: "lol".to_string(),
            chat: 1,
            tags: vec![],
            links: vec![],
        },
        Ok(1),
    );
    let uc = make_uc(&streamers_repo);
    assert_eq!(uc.execute(1, "/addstreamer lol"), Ok("Ok".to_string()))
}

#[test]
fn test_name_and_tags() {
    let streamers_repo = make_repos();
    streamers_repo.register.push_eq(
        NewStreamer {
            name: "lol".to_string(),
            chat: 1,
            tags: vec!["kek".to_string(), "cheb".to_string()],
            links: vec![],
        },
        Ok(1),
    );
    let uc = make_uc(&streamers_repo);
    assert_eq!(
        uc.execute(1, "/addstreamer lol,kek,cheb"),
        Ok("Ok".to_string())
    )
}

#[test]
fn test_descr() {
    let streamers_repo = make_repos();
    streamers_repo.register.push_eq(
        NewStreamer {
            name: "lol".to_string(),
            chat: 1,
            tags: vec![],
            links: vec!["wee".to_string()],
        },
        Ok(1),
    );
    let uc = make_uc(&streamers_repo);
    assert_eq!(
        uc.execute(1, "/addstreamer lol,\nwee"),
        Ok("Ok".to_string())
    )
}
