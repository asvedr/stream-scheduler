use chrono::{TimeZone, Utc};

use crate::entities::events::NewEvent;
use crate::entities::settings::Settings;
use crate::factories::events_repo::MockEventRepo;
use crate::factories::settings_repo::MockSettingsRepo;
use crate::factories::time_getter::MockTimeGetter;
use crate::impls::time_ui::TimeUI;
use crate::proto::robot::ITgUseCase;
use crate::tg_use_cases::create_event::CreateEventUC;

fn make_repos() -> (MockSettingsRepo, MockEventRepo) {
    let s_repo = MockSettingsRepo::default();
    let e_repo = MockEventRepo::default();
    s_repo.get.push_unchecked(Ok(Settings::default()));
    (s_repo, e_repo)
}

fn make_uc<'a, 'b>(
    settings_repo: &'a MockSettingsRepo,
    events_repo: &'b MockEventRepo,
) -> CreateEventUC<'a, 'b, TimeUI, MockTimeGetter> {
    let now = Utc.with_ymd_and_hms(2020, 5, 9, 11, 30, 1).unwrap();
    CreateEventUC {
        settings_repo,
        events_repo,
        time_ui: TimeUI,
        time_getter: MockTimeGetter { now },
    }
}

#[test]
fn test_empty() {
    let (s_repo, e_repo) = make_repos();
    let uc = make_uc(&s_repo, &e_repo);
    let cmd = uc.command();
    assert_eq!(uc.execute(1, cmd).unwrap(), "Event name not set")
}

#[test]
fn test_no_time() {
    let (s_repo, e_repo) = make_repos();
    let uc = make_uc(&s_repo, &e_repo);
    let cmd = uc.command();
    assert_eq!(uc.execute(1, "/addevent ds").unwrap(), "Event time not set")
}

#[test]
fn test_invalid_time() {
    let (s_repo, e_repo) = make_repos();
    let uc = make_uc(&s_repo, &e_repo);
    assert_eq!(uc.execute(1, "/addevent ds\nabcd").unwrap(), "Invalid time")
}

#[test]
fn test_valid_time() {
    let (s_repo, e_repo) = make_repos();
    e_repo.create_event.push_eq(
        NewEvent {
            name: "ds".to_string(),
            chat_id: 1,
            timezone: "utc".to_string(),
            time: Utc.with_ymd_and_hms(2020, 5, 9, 12, 30, 0).unwrap(),
            message: "".to_string(),
        },
        Ok(2),
    );
    e_repo
        .add_notif_times
        .push_eq((2, vec![60 * 60, 30 * 60, 10 * 60, 5 * 60, 60]), Ok(()));
    let uc = make_uc(&s_repo, &e_repo);
    assert_eq!(
        uc.execute(1, "/addevent ds\ntoday 12:30 utc").unwrap(),
        "Created: 2"
    )
}

#[test]
fn test_another_zone() {
    let (s_repo, e_repo) = make_repos();
    e_repo.create_event.push_eq(
        NewEvent {
            name: "ds".to_string(),
            chat_id: 1,
            timezone: "msk".to_string(),
            time: Utc.with_ymd_and_hms(2020, 5, 9, 15, 00, 0).unwrap(),
            message: "".to_string(),
        },
        Ok(2),
    );
    e_repo.add_notif_times.push_unchecked(Ok(()));
    let uc = make_uc(&s_repo, &e_repo);
    assert_eq!(
        uc.execute(1, "/addevent ds\ntoday 18:0 msk").unwrap(),
        "Created: 2"
    )
}

#[test]
fn test_time_in_past() {
    let (s_repo, e_repo) = make_repos();
    let uc = make_uc(&s_repo, &e_repo);
    assert_eq!(
        uc.execute(1, "/addevent ds\ntoday 10:30 msk").unwrap(),
        "Time can not be in past"
    )
}

#[test]
fn test_descr() {
    let (s_repo, e_repo) = make_repos();
    e_repo.create_event.push_eq(
        NewEvent {
            name: "ds".to_string(),
            chat_id: 1,
            timezone: "msk".to_string(),
            time: Utc.with_ymd_and_hms(2020, 5, 9, 15, 00, 0).unwrap(),
            message: "hehe".to_string(),
        },
        Ok(2),
    );
    e_repo.add_notif_times.push_unchecked(Ok(()));
    let uc = make_uc(&s_repo, &e_repo);
    assert_eq!(
        uc.execute(1, "/addevent ds\ntoday 18:0 msk\nhehe").unwrap(),
        "Created: 2"
    )
}
