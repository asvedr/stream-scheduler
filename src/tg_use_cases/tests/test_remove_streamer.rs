use crate::entities::errors::StreamerRepoError;
use crate::entities::streamers::Streamer;
use crate::factories::streamers_repo::MockStreamersRepo;
use crate::proto::robot::ITgUseCase;
use crate::tg_use_cases::remove_streamer::RemoveStreamerUC;

fn make_repos() -> MockStreamersRepo {
    MockStreamersRepo::default()
}

fn make_uc(streamers_repo: &MockStreamersRepo) -> RemoveStreamerUC {
    RemoveStreamerUC { streamers_repo }
}

#[test]
fn test_success() {
    let repo = make_repos();
    repo.get_by_name.push_eq(
        (1, "bep bop".to_string()),
        Ok(Streamer {
            id: 123,
            ..Default::default()
        }),
    );
    repo.remove.push_eq(123, Ok(()));
    let uc = make_uc(&repo);
    assert_eq!(uc.execute(1, "/rmstreamer bep bop  "), Ok("Ok".to_string()))
}

#[test]
fn test_nf() {
    let repo = make_repos();
    repo.get_by_name
        .push_eq((1, "bep bop".to_string()), Err(StreamerRepoError::NotFound));
    let uc = make_uc(&repo);
    assert_eq!(
        uc.execute(1, "/rmstreamer bep bop  "),
        Ok("Not found".to_string())
    )
}

#[test]
fn test_empty() {
    let repo = make_repos();
    let uc = make_uc(&repo);
    assert_eq!(
        uc.execute(1, "/rmstreamer  "),
        Ok("Name not set".to_string())
    )
}
