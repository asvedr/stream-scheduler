use crate::entities::errors::EventRepoError;
use crate::entities::events::{Event, NewEvent};
use crate::factories::events_repo::MockEventRepo;
use crate::proto::robot::ITgUseCase;
use crate::tg_use_cases::remove_event::RemoveEventUC;

fn make_repos() -> MockEventRepo {
    MockEventRepo::default()
}

fn make_uc(events_repo: &MockEventRepo) -> RemoveEventUC {
    RemoveEventUC { events_repo }
}

#[test]
fn test_empty() {
    let repo = make_repos();
    let uc = make_uc(&repo);
    assert_eq!(uc.execute(1, "/rmevent").unwrap(), "Id not set")
}

#[test]
fn test_nan() {
    let repo = make_repos();
    let uc = make_uc(&repo);
    assert_eq!(uc.execute(1, "/rmevent abc").unwrap(), "Invalid id")
}

#[test]
fn test_nf() {
    let repo = make_repos();
    repo.get_by_id.push_eq(1, Err(EventRepoError::NotFound));
    let uc = make_uc(&repo);
    assert_eq!(uc.execute(1, "/rmevent 1").unwrap(), "Event not found")
}

#[test]
fn test_bad_chat() {
    let repo = make_repos();
    let event = Event {
        id: 0,
        new: NewEvent {
            name: "".to_string(),
            chat_id: 2,
            timezone: "".to_string(),
            time: Default::default(),
            message: "".to_string(),
        },
    };
    repo.get_by_id.push_eq(1, Ok(event));
    let uc = make_uc(&repo);
    assert_eq!(uc.execute(1, "/rmevent 1").unwrap(), "Event not found")
}

#[test]
fn test_removed() {
    let repo = make_repos();
    let event = Event {
        id: 123,
        new: NewEvent {
            name: "".to_string(),
            chat_id: 1,
            timezone: "".to_string(),
            time: Default::default(),
            message: "".to_string(),
        },
    };
    repo.get_by_id.push_eq(123, Ok(event));
    repo.remove_event.push_eq(123, Ok(()));
    let uc = make_uc(&repo);
    assert_eq!(uc.execute(1, "/rmevent 123").unwrap(), "Ok")
}
