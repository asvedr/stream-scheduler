use crate::entities::streamers::{NewStreamer, Streamer};
use crate::factories::streamers_repo::MockStreamersRepo;
use crate::proto::robot::ITgUseCase;
use crate::tg_use_cases::show_streamers::ShowStreamersUC;

fn make_repos() -> MockStreamersRepo {
    MockStreamersRepo::default()
}

fn make_uc(streamers_repo: &MockStreamersRepo) -> ShowStreamersUC {
    ShowStreamersUC { streamers_repo }
}

#[test]
fn test_none() {
    let repo = make_repos();
    repo.get_all_for_chat.push_eq(1, Ok(Vec::new()));
    let uc = make_uc(&repo);
    let cmd = uc.command();
    assert_eq!(uc.execute(1, cmd).unwrap(), "No streamers found",)
}

#[test]
fn test_list() {
    let repo = make_repos();
    let list = vec![
        Streamer {
            id: 1,
            new: NewStreamer {
                name: "Aaa".to_string(),
                links: vec!["bep: bop".to_string()],
                ..Default::default()
            },
        },
        Streamer {
            id: 2,
            new: NewStreamer {
                name: "Bbb".to_string(),
                ..Default::default()
            },
        },
    ];
    repo.get_all_for_chat.push_eq(2, Ok(list));
    let uc = make_uc(&repo);
    let cmd = uc.command();
    assert_eq!(uc.execute(2, cmd).unwrap(), "Aaa:\n  bep: bop\n\nBbb:",)
}
