use crate::entities::errors::{StreamerRepoError, TgUseCaseError};
use crate::entities::streamers::Streamer;
use crate::proto::robot::ITgUseCase;
use crate::proto::streamers::IStreamersRepo;

pub struct ShowStreamerUC<'a> {
    pub streamers_repo: &'a dyn IStreamersRepo,
}

impl<'a> ShowStreamerUC<'a> {
    fn get_streamer(&self, chat: i64, token: &str) -> Result<Vec<Streamer>, TgUseCaseError> {
        match self.streamers_repo.get_by_name(chat, token) {
            Ok(val) => return Ok(vec![val]),
            Err(StreamerRepoError::NotFound) => (),
            Err(err) => return Err(err.into()),
        }
        let results = self.streamers_repo.get_by_tag(Some(chat), token)?;
        Ok(results)
    }
}

impl<'a> ITgUseCase for ShowStreamerUC<'a> {
    fn command(&self) -> &str {
        "/streamer"
    }

    fn help(&self) -> &str {
        "<name or tag>"
    }

    fn execute(&self, chat: i64, text: &str) -> Result<String, TgUseCaseError> {
        let token = text[self.command().len()..].trim().to_lowercase();
        if token.is_empty() {
            return Ok("Name not set".to_string());
        }
        let streamers = self.get_streamer(chat, &token)?;
        if streamers.is_empty() {
            return Ok("Not found".to_string());
        }
        let msg = streamers
            .iter()
            .map(|s| s.show())
            .collect::<Vec<_>>()
            .join("\n");
        Ok(msg.trim().to_string())
    }
}
