use crate::entities::errors::TgUseCaseError;
use crate::proto::robot::ITgUseCase;
use crate::proto::settings::ISettingsRepo;

pub struct RemoveTzUC<'a> {
    pub settings_repo: &'a dyn ISettingsRepo,
}

impl<'a> ITgUseCase for RemoveTzUC<'a> {
    fn command(&self) -> &str {
        "/rmtz"
    }

    fn help(&self) -> &str {
        "code: удалить временную зону"
    }

    fn execute(&self, _: i64, text: &str) -> Result<String, TgUseCaseError> {
        let code = text[self.command().len()..].trim();
        if code.is_empty() {
            return Ok("Code is not set".to_string());
        }
        let mut settings = self.settings_repo.get()?;
        if settings.get_tz(code).is_none() {
            return Ok("Tz not found".to_string());
        }
        if settings.protected_tz().contains(&code) {
            return Ok("Can not remove protected tz".to_string());
        }
        settings.remove_tz(code);
        self.settings_repo.update(settings)?;
        Ok("Ok".to_string())
    }
}
