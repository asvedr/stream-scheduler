use crate::entities::errors::TgUseCaseError;
use crate::proto::robot::ITgUseCase;
use crate::proto::streamers::IStreamersRepo;

pub struct ShowStreamersUC<'a> {
    pub streamers_repo: &'a dyn IStreamersRepo,
}

impl<'a> ITgUseCase for ShowStreamersUC<'a> {
    fn command(&self) -> &str {
        "/streamers"
    }

    fn help(&self) -> &str {
        ": показать всех стримеров в чате"
    }

    fn execute(&self, chat: i64, _: &str) -> Result<String, TgUseCaseError> {
        let streamers = self.streamers_repo.get_all_for_chat(chat)?;
        if streamers.is_empty() {
            return Ok("No streamers found".to_string());
        }
        let msg = streamers
            .iter()
            .map(|s| s.show())
            .collect::<Vec<_>>()
            .join("\n");
        Ok(msg.trim().to_string())
    }
}
