use crate::entities::errors::TgUseCaseError;
use crate::proto::robot::ITgUseCase;

pub struct HelpUC {
    pub command_factory: &'static dyn Fn() -> &'static Vec<&'static dyn ITgUseCase>,
}

impl ITgUseCase for HelpUC {
    fn command(&self) -> &str {
        "/help"
    }

    fn help(&self) -> &str {
        ": показать это сообщение"
    }

    fn execute(&self, _: i64, _: &str) -> Result<String, TgUseCaseError> {
        let ucs = (self.command_factory)();
        let text = ucs
            .iter()
            .map(|uc| format!("{} {}", uc.command(), uc.help()))
            .collect::<Vec<_>>()
            .join("\n");
        Ok(text)
    }
}
