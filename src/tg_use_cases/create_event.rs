use chrono::{
    DateTime, Datelike, FixedOffset, LocalResult, NaiveDate, NaiveDateTime, TimeZone, Timelike, Utc,
};

use crate::entities::errors::{TgUseCaseError, TimeError};
use crate::entities::events::NewEvent;
use crate::proto::common::{ITimeGetter, ITimeUI};
use crate::proto::events::IEventRepo;
use crate::proto::robot::ITgUseCase;
use crate::proto::settings::ISettingsRepo;
use crate::utils::time::tz_to_utc;

pub struct CreateEventUC<'a, 'b, TU: ITimeUI, TG: ITimeGetter> {
    pub settings_repo: &'a dyn ISettingsRepo,
    pub events_repo: &'b dyn IEventRepo,
    pub time_ui: TU,
    pub time_getter: TG,
}

struct Input {
    name: String,
    time: NaiveDateTime,
    tz_code: String,
    message: String,
}

impl<'a, 'b, TU: ITimeUI, TG: ITimeGetter> CreateEventUC<'a, 'b, TU, TG> {
    fn parse_input(
        &self,
        src: &str,
        default_tz: &str,
        default_date: NaiveDate,
    ) -> Result<Input, String> {
        let rows = src
            .split('\n')
            .filter(|s| !s.trim().is_empty())
            .collect::<Vec<_>>();
        if rows.is_empty() {
            return Err("Event name not set".to_string());
        }
        let name = rows[0].trim().to_string();
        if rows.len() < 2 {
            return Err("Event time not set".to_string());
        }
        let res_time = self
            .time_ui
            .parse_time(rows[1].trim(), default_date, default_tz);
        let (time, tz_code) = match res_time {
            Ok(val) => val,
            Err(TimeError::InvalidTime) => return Err("Invalid time".to_string()),
            Err(TimeError::InvalidDate) => return Err("Invalid date".to_string()),
            Err(TimeError::TimeNotSet) => return Err("Time not set".to_string()),
        };
        let message = rows[2..].join("\n");
        Ok(Input {
            name,
            time,
            tz_code,
            message,
        })
    }

    fn build_time(time: NaiveDateTime, tz: FixedOffset) -> DateTime<Utc> {
        let res_local = tz.with_ymd_and_hms(
            time.year(),
            time.month(),
            time.day(),
            time.hour(),
            time.minute(),
            time.second(),
        );
        let local = match res_local {
            LocalResult::Single(val) => val,
            _ => panic!("Naive to TZ error: {:?}", time),
        };
        tz_to_utc(local)
    }

    fn build_event(tz: FixedOffset, chat_id: i64, input: Input) -> NewEvent {
        let time = Self::build_time(input.time, tz);
        NewEvent {
            name: input.name,
            chat_id,
            timezone: input.tz_code,
            time,
            message: input.message,
        }
    }
}

impl<'a, 'b, TU: ITimeUI, TG: ITimeGetter> ITgUseCase for CreateEventUC<'a, 'b, TU, TG> {
    fn command(&self) -> &str {
        "/addevent"
    }

    fn help(&self) -> &str {
        concat!(
            "\n  _аргументы_: event-name <enter> time <enter>[msg]\n",
            "    time: [date] hh:mm [tz]\n",
            "    date: [[hh-]mm-]dd/сегодня/завтра/послезавтра\n",
            "  _описание_: запланировать стрим"
        )
    }

    fn execute(&self, chat: i64, text: &str) -> Result<String, TgUseCaseError> {
        let settings = self.settings_repo.get()?;
        let now = self.time_getter.now();
        let input_res = self.parse_input(
            text[self.command().len()..].trim(),
            &settings.default_timezone,
            now.date_naive(),
        );
        let input = match input_res {
            Ok(val) => val,
            Err(msg) => return Ok(msg),
        };
        let tz = match settings.get_tz(&input.tz_code) {
            Some(tz) => tz,
            _ => return Ok("Invalid timezone".to_string()),
        };
        let event = Self::build_event(tz, chat, input);
        println!(">> {:?} {:?}", event.time, now);
        if event.time < now {
            return Ok("Time can not be in past".to_string());
        }
        let id = self.events_repo.create_event(event)?;
        self.events_repo
            .add_notif_times(id, &settings.seconds_before())?;
        Ok(format!("Created: {}", id))
    }
}
