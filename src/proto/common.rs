use crate::entities::errors::TimeError;
use chrono::{DateTime, FixedOffset, NaiveDate, NaiveDateTime, Utc};

pub trait ITimeUI {
    fn parse_time(
        &self,
        text: &str,
        default_date: NaiveDate,
        default_code: &str,
    ) -> Result<(NaiveDateTime, String), TimeError>;

    fn show_time(
        &self,
        time: DateTime<Utc>,
        tz_list: &[(&str, FixedOffset)],
        now: DateTime<Utc>,
    ) -> String;
}

pub trait ITimeGetter {
    fn now(&self) -> DateTime<Utc>;
}
