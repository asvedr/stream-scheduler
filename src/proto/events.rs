use crate::entities::errors::EventRepoError;

use crate::entities::events::{Event, EventWithSeconds, NewEvent};

pub trait IEventRepo {
    fn create_event(&self, event: NewEvent) -> Result<i64, EventRepoError>;
    fn update_event(&self, event: Event) -> Result<(), EventRepoError>;
    fn add_notif_times(
        &self,
        event_id: i64,
        seconds_before: &[usize],
    ) -> Result<(), EventRepoError>;
    fn remove_event(&self, event_id: i64) -> Result<(), EventRepoError>;
    fn get_by_id(&self, event_id: i64) -> Result<Event, EventRepoError>;
    fn get_chat_events(
        &self,
        chat_id: i64,
        filter_expired: bool,
    ) -> Result<Vec<Event>, EventRepoError>;
    fn get_events_to_notify(&self) -> Result<Vec<EventWithSeconds>, EventRepoError>;
    fn set_send_notifications(
        &self,
        event_id: i64,
        seconds_before: &[usize],
    ) -> Result<(), EventRepoError>;
    fn remove_old_events(&self) -> Result<(), EventRepoError>;
}
