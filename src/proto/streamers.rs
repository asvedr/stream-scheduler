use crate::entities::errors::StreamerRepoError;
use crate::entities::streamers::{NewStreamer, Streamer};

pub trait IStreamersRepo {
    fn register(&self, streamer: NewStreamer) -> Result<i64, StreamerRepoError>;
    fn remove(&self, streamer_id: i64) -> Result<(), StreamerRepoError>;
    fn get_by_id(&self, id: i64) -> Result<Streamer, StreamerRepoError>;
    fn get_by_name(&self, chat: i64, name: &str) -> Result<Streamer, StreamerRepoError>;
    fn get_all_for_chat(&self, chat: i64) -> Result<Vec<Streamer>, StreamerRepoError>;
    fn get_all(&self) -> Result<Vec<Streamer>, StreamerRepoError>;
    fn get_by_tag(&self, chat: Option<i64>, text: &str)
        -> Result<Vec<Streamer>, StreamerRepoError>;
}
