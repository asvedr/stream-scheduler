use crate::entities::errors::{RobotError, TgUseCaseError};

pub trait ITgUseCase {
    fn command(&self) -> &str;
    fn help(&self) -> &str;
    fn execute(&self, chat: i64, text: &str) -> Result<String, TgUseCaseError>;
}

pub trait IRobot {
    fn name(&self) -> &str;
    fn run(&mut self) -> Result<(), RobotError>;
}
