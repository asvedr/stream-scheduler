use crate::entities::errors::SettingsRepoError;
use crate::entities::settings::Settings;

pub trait ISettingsRepo {
    fn get(&self) -> Result<Settings, SettingsRepoError>;
    fn get_raw(&self) -> Result<Vec<(String, String)>, SettingsRepoError>;
    fn update(&self, settings: Settings) -> Result<(), SettingsRepoError>;
    fn raw_to_settings(&self, raw: Vec<(String, String)>) -> Result<Settings, SettingsRepoError>;
    fn settings_to_raw(&self, settings: Settings) -> Vec<(String, String)>;
}
