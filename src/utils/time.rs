use chrono::{DateTime, FixedOffset, TimeZone, Utc};

pub fn utc_to_int(time: DateTime<Utc>) -> i64 {
    time.timestamp()
}

pub fn int_to_utc(secs: i64) -> DateTime<Utc> {
    Utc.timestamp_opt(secs, 0).unwrap()
}

pub fn utc_to_tz(time: DateTime<Utc>, tz: FixedOffset) -> DateTime<FixedOffset> {
    let secs = time.timestamp();
    tz.timestamp_opt(secs, 0).unwrap()
}

pub fn tz_to_utc(time: DateTime<FixedOffset>) -> DateTime<Utc> {
    let secs = time.timestamp();
    Utc.timestamp_opt(secs, 0).unwrap()
}
