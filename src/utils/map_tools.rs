use std::collections::HashMap;
use std::hash::Hash;

pub fn add<K: Eq + Hash, V>(map: &mut HashMap<K, Vec<V>>, key: K, val: V) {
    if let Some(list) = map.get_mut(&key) {
        list.push(val);
        return;
    }
    map.insert(key, vec![val]);
}
