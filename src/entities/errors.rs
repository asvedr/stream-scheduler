use easy_sqlite::errors::DbError;
use std::io::Error;

#[derive(Debug, Eq, PartialEq)]
pub enum EventRepoError {
    DbError(DbError),
    NotFound,
}

#[derive(Debug, Eq, PartialEq)]
pub enum SettingsRepoError {
    DbError(DbError),
    InvalidValueAtKey(&'static str),
}

#[derive(Debug, Eq, PartialEq)]
pub enum StreamerRepoError {
    DbError(DbError),
    NotFound,
}

#[derive(Debug, Eq, PartialEq)]
pub enum TgUseCaseError {
    Streamer(StreamerRepoError),
    Settings(SettingsRepoError),
    Events(EventRepoError),
}

#[derive(Debug)]
pub enum TimeError {
    TimeNotSet,
    InvalidDate,
    InvalidTime,
}

#[derive(Debug)]
pub enum RobotError {
    Tg(telegram_bot_ars::Error),
    SettingsRepo(SettingsRepoError),
    EventsRepo(EventRepoError),
    StreamerRepo(StreamerRepoError),
    IO(std::io::Error),
    Time(TimeError),
}

impl From<DbError> for EventRepoError {
    fn from(value: DbError) -> Self {
        match value {
            DbError::NotFound(_) => Self::NotFound,
            other => Self::DbError(other),
        }
    }
}

impl From<DbError> for SettingsRepoError {
    fn from(value: DbError) -> Self {
        Self::DbError(value)
    }
}

impl From<DbError> for StreamerRepoError {
    fn from(value: DbError) -> Self {
        if matches!(value, DbError::NotFound(_)) {
            return Self::NotFound;
        }
        Self::DbError(value)
    }
}

impl From<StreamerRepoError> for TgUseCaseError {
    fn from(value: StreamerRepoError) -> Self {
        Self::Streamer(value)
    }
}

impl From<SettingsRepoError> for TgUseCaseError {
    fn from(value: SettingsRepoError) -> Self {
        Self::Settings(value)
    }
}

impl From<EventRepoError> for TgUseCaseError {
    fn from(value: EventRepoError) -> Self {
        Self::Events(value)
    }
}

impl From<telegram_bot_ars::Error> for RobotError {
    fn from(value: telegram_bot_ars::Error) -> Self {
        Self::Tg(value)
    }
}

impl From<SettingsRepoError> for RobotError {
    fn from(value: SettingsRepoError) -> Self {
        Self::SettingsRepo(value)
    }
}

impl From<EventRepoError> for RobotError {
    fn from(value: EventRepoError) -> Self {
        Self::EventsRepo(value)
    }
}

impl From<StreamerRepoError> for RobotError {
    fn from(value: StreamerRepoError) -> Self {
        Self::StreamerRepo(value)
    }
}

impl From<std::io::Error> for RobotError {
    fn from(value: Error) -> Self {
        Self::IO(value)
    }
}

impl From<TimeError> for RobotError {
    fn from(value: TimeError) -> Self {
        Self::Time(value)
    }
}
