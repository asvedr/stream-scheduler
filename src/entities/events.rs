use chrono::{DateTime, Utc};

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct NewEvent {
    pub name: String,
    pub chat_id: i64,
    pub timezone: String,
    pub time: DateTime<Utc>,
    pub message: String,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Event {
    pub id: i64,
    pub new: NewEvent,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct EventWithSeconds {
    pub event: Event,
    pub seconds_before: Vec<usize>,
}
