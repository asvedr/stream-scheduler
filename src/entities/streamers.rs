#[derive(Debug, Eq, PartialEq, Clone, Default)]
pub struct NewStreamer {
    pub name: String,
    pub chat: i64,
    pub tags: Vec<String>,
    pub links: Vec<String>,
}

#[derive(Debug, Eq, PartialEq, Clone, Default)]
pub struct Streamer {
    pub id: i64,
    pub new: NewStreamer,
}

fn uppercase_name(name: &str) -> String {
    let mut chars = name.chars();
    let mut result = match chars.next() {
        None => return "".to_string(),
        Some(c) => c.to_uppercase().collect::<String>(),
    };
    result.push_str(&chars.collect::<String>());
    result
}

impl Streamer {
    pub fn show(&self) -> String {
        let mut result = format!("{}:\n", uppercase_name(&self.new.name));
        for link in self.new.links.iter() {
            result.push_str("  ");
            result.push_str(link);
            result.push('\n');
        }
        result
    }
}
