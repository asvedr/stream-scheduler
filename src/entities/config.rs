use mddd::macros::IConfig;

#[allow(clippy::manual_non_exhaustive)]
#[derive(IConfig)]
pub struct Config {
    #[description("telegram token")]
    pub token: String,
    #[description("path to sqlite base")]
    #[default("scheduler.db".to_string())]
    pub db: String,
    #[description("timeout before restart broken thread(secs)")]
    #[default(5)]
    pub restart_timeout: u64,
    #[description("sleep in seconds")]
    #[default(2.0)]
    pub notifier_check_sleep: f64,
    #[description("sleep between tg msg sending")]
    #[default(0.8)]
    pub notifier_send_sleep: f64,
    #[description("max tg retries for batch")]
    #[default(10)]
    pub max_tg_retries: usize,
    #[prefix("STREAM_SCHEDULER_")]
    __meta__: (),
}
