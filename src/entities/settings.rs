use chrono::FixedOffset;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Settings {
    pub timezones: Vec<(String, i32)>,
    pub display_timezones: Vec<String>,
    pub default_timezone: String,
    pub local_timezone: String,
    pub notif_shifts_in_minutes: Vec<usize>,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            timezones: vec![
                ("utc".to_string(), 0),
                ("msk".to_string(), 3),
                ("cy".to_string(), 2),
            ],
            display_timezones: vec!["msk".to_string(), "cy".to_string()],
            default_timezone: "msk".to_string(),
            local_timezone: "cy".to_string(),
            notif_shifts_in_minutes: vec![60, 30, 10, 5, 1],
        }
    }
}

const HOUR: i32 = 60 * 60;

impl Settings {
    pub fn get_tz(&self, code: &str) -> Option<FixedOffset> {
        let index = self.tz_index(code)?;
        let (_, ref shift) = &self.timezones[index];
        FixedOffset::east_opt(*shift * HOUR)
    }

    pub fn update_tz(&mut self, code: &str, shift: i32) {
        if let Some(index) = self.tz_index(code) {
            let (_, ref mut dst) = &mut self.timezones[index];
            *dst = shift;
        } else {
            self.timezones.push((code.to_string(), shift))
        }
    }

    pub fn remove_tz(&mut self, code: &str) {
        let index = match self.tz_index(code) {
            None => return,
            Some(val) => val,
        };
        self.timezones.remove(index);
    }

    pub fn protected_tz(&self) -> Vec<&str> {
        let mut result = vec!["utc", &self.default_timezone, &self.local_timezone];
        for tz in self.display_timezones.iter() {
            result.push(tz);
        }
        result
    }

    pub fn seconds_before(&self) -> Vec<usize> {
        self.notif_shifts_in_minutes
            .iter()
            .map(|n| n * 60)
            .collect()
    }

    fn tz_index(&self, code: &str) -> Option<usize> {
        for (i, (tz_code, _)) in self.timezones.iter().enumerate() {
            if code == tz_code {
                return Some(i);
            }
        }
        None
    }

    pub fn get_display_timezones(&self) -> Vec<(&str, FixedOffset)> {
        let mut result: Vec<(&str, FixedOffset)> = Vec::new();
        for code in self.display_timezones.iter() {
            result.push((code, self.get_tz(code).unwrap()))
        }
        result
    }
}
