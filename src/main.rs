mod app;
mod cli_use_cases;
mod entities;
#[cfg(test)]
mod factories;
mod impls;
mod proto;
mod tg_use_cases;
mod utils;

fn main() {
    use crate::app::cli_use_cases as cluc;
    use mddd::std::{LeafRunner, NodeRunner};

    let db_tools = NodeRunner::new("db tools")
        .add(
            "settings",
            NodeRunner::new("settings table")
                .add("get", LeafRunner::new(cluc::get_settings))
                .add("upd", LeafRunner::new(cluc::upd_settings)),
        )
        .add(
            "streamers",
            NodeRunner::new("streamers table")
                .add("get", LeafRunner::new(cluc::get_streamers))
                .add("add", LeafRunner::new(cluc::add_streamer))
                .add("rm", LeafRunner::new(cluc::rm_streamer)),
        )
        .add(
            "events",
            NodeRunner::new("events table")
                .add("get", LeafRunner::new(cluc::get_events))
                .add("add", LeafRunner::new(cluc::add_event))
                .add("rm-old", LeafRunner::new(cluc::rm_old_events))
                .add("rm", LeafRunner::new(cluc::rm_event)),
        );
    NodeRunner::new("Telegram stream scheduler")
        .add("config", LeafRunner::new(cluc::config_help))
        .add("run", LeafRunner::new(cluc::run))
        .add("db", db_tools)
        .set_default_params(&["run"])
        .run()
}
