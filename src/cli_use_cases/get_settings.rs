use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::proto::settings::ISettingsRepo;

pub struct GetSettingsUC<'a> {
    pub settings_repo: &'a dyn ISettingsRepo,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    key: Option<String>,
}

impl<'a> GetSettingsUC<'a> {
    fn find_row<'b>(rows: &'b [(String, String)], key: &str) -> Option<&'b str> {
        let (_, val) = rows.iter().find(|(r_key, _)| r_key == key)?;
        Some(val)
    }

    fn combine(
        value_rows: Vec<(String, String)>,
        db_rows: Vec<(String, String)>,
    ) -> Vec<(bool, String, String)> {
        let mut result = Vec::new();
        for (key, val) in value_rows {
            let row = if let Some(db_val) = Self::find_row(&db_rows, &key) {
                (false, key, db_val.to_string())
            } else {
                (true, key, val)
            };
            result.push(row)
        }
        result
    }

    fn print(rows: Vec<(bool, String, String)>) {
        for (dflt, key, val) in rows {
            let mark = if dflt { 'y' } else { 'n' };
            println!("  dflt={}, key={}, val={:?}", mark, key, val);
        }
    }
}

impl<'a> IUseCase for GetSettingsUC<'a> {
    type Request = Request;
    type Error = RobotError;

    fn short_description() -> String {
        "show db settings".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        let settings = self.settings_repo.get()?;
        let deser_rows = self.settings_repo.settings_to_raw(settings);
        let db_rows = self.settings_repo.get_raw()?;
        let rows = Self::combine(deser_rows, db_rows);
        if let Some(key) = request.key {
            let filtered = rows
                .iter()
                .filter(|(_, row_key, _)| row_key.contains(&key))
                .cloned()
                .collect::<Vec<_>>();
            Self::print(filtered)
        } else {
            Self::print(rows)
        }
        Ok(())
    }
}
