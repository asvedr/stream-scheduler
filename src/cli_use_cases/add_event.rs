use chrono::{Datelike, LocalResult, TimeZone, Timelike};
use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;
use std::io::Read;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::entities::events::NewEvent;
use crate::proto::common::{ITimeGetter, ITimeUI};
use crate::proto::events::IEventRepo;
use crate::proto::settings::ISettingsRepo;
use crate::utils::time::tz_to_utc;

pub struct AddEventUC<'a, 'b, TG, TU> {
    pub settings_repo: &'a dyn ISettingsRepo,
    pub events_repo: &'b dyn IEventRepo,
    pub time_getter: TG,
    pub time_ui: TU,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    #[short("-n")]
    name: String,
    #[short("-c")]
    chat: i64,
    #[short("-t")]
    time: String,
    #[bool_flag]
    #[short("-m")]
    read_message_from_stdin: bool,
}

impl<'a, 'b, TG: ITimeGetter, TU: ITimeUI> IUseCase for AddEventUC<'a, 'b, TG, TU> {
    type Request = Request;
    type Error = RobotError;

    fn short_description() -> String {
        "add new event".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        let mut message = String::new();
        if request.read_message_from_stdin {
            std::io::stdin().read_to_string(&mut message)?;
        }
        let today = self.time_getter.now().date_naive();
        let settings = self.settings_repo.get()?;
        let (naive_time, tz_code) =
            self.time_ui
                .parse_time(&request.time, today, &settings.default_timezone)?;
        let tz = match settings.get_tz(&tz_code) {
            None => {
                eprintln!("Error: invalid tz {:?}", tz_code);
                std::process::exit(1)
            }
            Some(val) => val,
        };
        let res_local = tz.with_ymd_and_hms(
            naive_time.year(),
            naive_time.month(),
            naive_time.day(),
            naive_time.hour(),
            naive_time.minute(),
            naive_time.second(),
        );
        let time = match res_local {
            LocalResult::Single(val) => val,
            _ => {
                eprintln!("Error: invalid time: {:?}", request.time);
                std::process::exit(1)
            }
        };
        let event = NewEvent {
            name: request.name,
            chat_id: request.chat,
            timezone: tz_code,
            time: tz_to_utc(time),
            message: message.trim_end().to_string(),
        };
        let id = self.events_repo.create_event(event)?;
        println!("ok: {}", id);
        Ok(())
    }
}
