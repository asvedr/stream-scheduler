use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;
use std::io::Read;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::entities::streamers::NewStreamer;
use crate::proto::streamers::IStreamersRepo;

pub struct AddStreamerUC<'a> {
    pub streamers_repo: &'a dyn IStreamersRepo,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    #[short("-n")]
    name: String,
    #[short("-c")]
    chat: i64,
    #[multi]
    #[short("-t")]
    tags: Vec<String>,
    #[bool_flag]
    #[short("-d")]
    read_data_from_stdin: bool,
}

impl<'a> IUseCase for AddStreamerUC<'a> {
    type Request = Request;
    type Error = RobotError;

    fn short_description() -> String {
        "add new streamer".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        let mut data = String::new();
        if request.read_data_from_stdin {
            std::io::stdin().read_to_string(&mut data)?;
        }
        let streamer = NewStreamer {
            name: request.name,
            chat: request.chat,
            tags: request.tags,
            links: data
                .trim_end()
                .split('\n')
                .map(|line| line.to_string())
                .collect(),
        };
        self.streamers_repo.register(streamer)?;
        println!("ok");
        Ok(())
    }
}
