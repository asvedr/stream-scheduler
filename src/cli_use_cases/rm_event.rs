use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::proto::events::IEventRepo;

pub struct RmEventUC<'a> {
    pub events_repo: &'a dyn IEventRepo,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    id: i64,
}

impl<'a> IUseCase for RmEventUC<'a> {
    type Request = Request;
    type Error = RobotError;

    fn short_description() -> String {
        "remove event by id".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        let _ = self.events_repo.get_by_id(request.id)?;
        self.events_repo.remove_event(request.id)?;
        println!("ok");
        Ok(())
    }
}
