use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::proto::settings::ISettingsRepo;

pub struct UpdSettingsUC<'a> {
    pub settings_repo: &'a dyn ISettingsRepo,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    #[bool_flag]
    #[short("-r")]
    reset_to_default: bool,
    key: String,
    value: Option<String>,
}

impl<'a> UpdSettingsUC<'a> {
    fn remove_mode(&self, key: String) -> Result<(), RobotError> {
        let mut rows = self.settings_repo.get_raw()?;
        if let Some(index) = Self::get_index(&rows, &key) {
            rows.remove(index);
        }
        let settings = self.settings_repo.raw_to_settings(rows)?;
        self.settings_repo.update(settings)?;
        println!("ok");
        Ok(())
    }

    fn update_mode(&self, key: String, value: String) -> Result<(), RobotError> {
        let mut rows = self.settings_repo.get_raw()?;
        Self::update_or_insert(&mut rows, key, value);
        let settings = self.settings_repo.raw_to_settings(rows)?;
        self.settings_repo.update(settings)?;
        println!("ok");
        Ok(())
    }

    fn get_index(rows: &[(String, String)], key: &str) -> Option<usize> {
        let (index, _) = rows
            .iter()
            .enumerate()
            .find(|(_, (r_key, _))| r_key == key)?;
        Some(index)
    }

    fn update_or_insert(rows: &mut Vec<(String, String)>, key: String, value: String) {
        if let Some(index) = Self::get_index(rows, &key) {
            rows[index] = (key, value)
        } else {
            rows.push((key, value))
        }
    }
}

impl<'a> IUseCase for UpdSettingsUC<'a> {
    type Request = Request;
    type Error = RobotError;

    fn short_description() -> String {
        "set settings field".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        if request.reset_to_default {
            return self.remove_mode(request.key);
        }
        if let Some(value) = request.value {
            return self.update_mode(request.key, value);
        }
        eprintln!("Error: insert mode requires a value");
        std::process::exit(1);
    }
}
