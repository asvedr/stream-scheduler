use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::proto::streamers::IStreamersRepo;

pub struct RmStreamerUC<'a> {
    pub streamers_repo: &'a dyn IStreamersRepo,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    id: i64,
}

impl<'a> IUseCase for RmStreamerUC<'a> {
    type Request = Request;
    type Error = RobotError;

    fn short_description() -> String {
        "remove streamer by id".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        let _ = self.streamers_repo.get_by_id(request.id)?;
        self.streamers_repo.remove(request.id)?;
        println!("ok");
        Ok(())
    }
}
