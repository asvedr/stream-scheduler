use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::entities::streamers::Streamer;
use crate::proto::streamers::IStreamersRepo;

pub struct GetStreamerUC<'a> {
    pub streamers_repo: &'a dyn IStreamersRepo,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    #[short("-id")]
    pub id: Option<i64>,
    #[short("-c")]
    pub chat: Option<i64>,
    #[short("-n")]
    pub name: Option<String>,
    #[short("-f")]
    #[bool_flag]
    pub show_full_info: bool,
}

impl<'a> GetStreamerUC<'a> {
    fn find(&self, request: &Request) -> Result<Vec<Streamer>, RobotError> {
        if let Some(id) = request.id {
            let streamer = self.streamers_repo.get_by_id(id)?;
            return Ok(vec![streamer]);
        }
        match request.chat {
            Some(val) => self.find_with_chat(val, request),
            _ => self.find_without_chat(request),
        }
    }

    fn find_with_chat(&self, chat: i64, request: &Request) -> Result<Vec<Streamer>, RobotError> {
        if let Some(ref tag) = request.name {
            Ok(self.streamers_repo.get_by_tag(Some(chat), tag)?)
        } else {
            Ok(self.streamers_repo.get_all_for_chat(chat)?)
        }
    }

    fn find_without_chat(&self, request: &Request) -> Result<Vec<Streamer>, RobotError> {
        if let Some(ref tag) = request.name {
            Ok(self.streamers_repo.get_by_tag(None, tag)?)
        } else {
            Ok(self.streamers_repo.get_all()?)
        }
    }

    fn print(streamer: Streamer, show_full: bool) {
        let new = streamer.new;
        println!(
            "id={id}\n  name: {name}\n  chat: {chat}\n  tags: {tags}",
            id = streamer.id,
            name = new.name,
            chat = new.chat,
            tags = new.tags.join(", ")
        );
        if show_full {
            println!("  info: {}", new.links.join("\\n"));
        }
    }
}

impl<'a> IUseCase for GetStreamerUC<'a> {
    type Request = Request;
    type Error = RobotError;

    fn short_description() -> String {
        "find streamer by id, chat or name".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        for streamer in self.find(&request)? {
            Self::print(streamer, request.show_full_info)
        }
        Ok(())
    }
}
