use mddd::traits::IUseCase;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::proto::events::IEventRepo;

pub struct RmOldEventsUC<'a> {
    pub events_repo: &'a dyn IEventRepo,
}

impl<'a> IUseCase for RmOldEventsUC<'a> {
    type Request = ();
    type Error = RobotError;

    fn short_description() -> String {
        "remove expired events".to_string()
    }

    fn execute(&mut self, _: Self::Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        self.events_repo.remove_old_events()?;
        Ok(())
    }
}
