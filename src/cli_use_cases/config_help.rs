use mddd::traits::{IConfig, IUseCase};

use crate::entities::config::Config;

pub struct ConfigHelpUC;

impl IUseCase for ConfigHelpUC {
    type Request = ();
    type Error = ();

    fn short_description() -> String {
        "show required environment variables".to_string()
    }

    fn execute(&mut self, _: Self::Request) -> Result<(), Self::Error> {
        println!("{}", Config::help());
        Ok(())
    }
}
