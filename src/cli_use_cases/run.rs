use mddd::traits::IUseCase;

use crate::app;

pub struct RunUC;

impl IUseCase for RunUC {
    type Request = ();
    type Error = ();

    fn short_description() -> String {
        "run telegram robot".to_string()
    }

    fn execute(&mut self, _: Self::Request) -> Result<(), Self::Error> {
        app::launcher::Launcher::default()
            .init_repos(app::repos::all())
            .add_robot(app::robots::ui())
            .add_robot(app::robots::notify())
            .wait();
        Ok(())
    }
}
