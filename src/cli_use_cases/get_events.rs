use chrono::{DateTime, FixedOffset, Utc};
use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::app::launcher::Launcher;
use crate::app::repos;
use crate::entities::errors::RobotError;
use crate::entities::events::NewEvent;
use crate::proto::common::{ITimeGetter, ITimeUI};
use crate::proto::events::IEventRepo;
use crate::proto::settings::ISettingsRepo;

pub struct GetEventsUC<'a, 'b, TG, TU> {
    pub settings_repo: &'a dyn ISettingsRepo,
    pub events_repo: &'b dyn IEventRepo,
    pub time_getter: TG,
    pub time_ui: TU,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    chat: i64,
}

impl<'a, 'b, TG: ITimeGetter, TU: ITimeUI> GetEventsUC<'a, 'b, TG, TU> {
    fn show(&self, tz_name: &str, tz: FixedOffset, now: DateTime<Utc>, event: NewEvent) -> String {
        let status = if event.time > now {
            "actual"
        } else {
            "expired"
        };
        let time = self.time_ui.show_time(event.time, &[(tz_name, tz)], now);
        [
            ("status", status),
            ("name", &event.name),
            ("tz", &event.timezone),
            ("time", &time),
        ]
        .into_iter()
        .map(|(k, v)| format!("  {}: {}", k, v))
        .collect::<Vec<_>>()
        .join("\n")
    }
}

impl<'a, 'b, TG: ITimeGetter, TU: ITimeUI> IUseCase for GetEventsUC<'a, 'b, TG, TU> {
    type Request = Request;
    type Error = RobotError;

    fn short_description() -> String {
        "get events".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        Launcher::default().init_repos(repos::all());
        let events = self.events_repo.get_chat_events(request.chat, false)?;
        if events.is_empty() {
            println!("no events found");
            return Ok(());
        }
        let settings = self.settings_repo.get()?;
        let tz_name = &settings.local_timezone;
        let tz = settings.get_tz(tz_name).unwrap();
        let now = self.time_getter.now();
        for event in events {
            let new = event.new;
            println!("id={}:\n{}", event.id, self.show(tz_name, tz, now, new));
        }
        Ok(())
    }
}
